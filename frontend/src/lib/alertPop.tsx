import {Alert, AlertTitle} from '@mui/material';
import useAlert from "@/lib/alert.tsx";



const AlertPopup = () => {
    const { text, type } = useAlert();

    if (text && type) {
        return (
            <Alert

                severity={type === 'error' ? 'error' : 'success'}
                sx={{
                    // position: 'absolute',
                    zIndex: 10,
                }}
                variant="outlined"
            >
                <AlertTitle>Response</AlertTitle>
                {text}
            </Alert>
        );
    } else {
        return <></>;
    }
};

export default AlertPopup;