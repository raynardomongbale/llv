import {SetStateAction, createContext, useState} from 'react';

const ALERT_TIME = 5000;
const initialState = {
    text: '',
    type: '',
};

const AlertContext = createContext({
    ...initialState,
    setAlert: (text:SetStateAction<string>, type: SetStateAction<string>) => {
        console.log('setAlert function triggered', text, type);
    },
});

// eslint-disable-next-line react/prop-types,@typescript-eslint/ban-ts-comment
// @ts-expect-error
// eslint-disable-next-line react/prop-types
export const AlertProvider = ({children}) => {


    const [text, setText] = useState('');
    const [type, setType] = useState('');

    const setAlert = (text: SetStateAction<string>, type: SetStateAction<string>) => {
        setText(text);
        setType(type);

        setTimeout(() => {
            setText('');
            setType('');
        }, ALERT_TIME);
    };

    return (
        <AlertContext.Provider
            value={{
                text,
                type,
                setAlert,
            }}
        >
            {children}
        </AlertContext.Provider>
    );
};

export default AlertContext;


