


export interface User {
    email: string
    username: string
    password: string
    organization: string
}

export type UserType = {
    id: string
    title: string
    username: string
    firstname: string
    surname: string
    email: string
    organization: string
    town: string
    country: string
    sex: string
    telephone: string
    postalcode: string
    password: string
    isadmin: boolean
}



export type CourseType = {
    id: string
    name: string
    size: number
    enrolled: boolean
    description: string
    startdate: string
    enddate: string
    final_registration_date: string
    town: string
    country: string
    state: string
    price: string
    currency: string
    organization: string
}

export type Booking = {
   bookingId: number
   name: string
   size: number
   description: string
   startdate: Date
   enddate: Date
   finalregistrationdate: Date
   state: string
   title: string
   username: string
   firstname: string
   surname: string
   email: string
   organization: string
   sex: string
   telephone: string
   postalcode: string
   password: string
   isadmin: boolean
}
