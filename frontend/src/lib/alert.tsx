import {useContext} from "react";
import AlertContext from "@/lib/alertContext.tsx";

const useAlert = () => useContext(AlertContext);

export default useAlert;