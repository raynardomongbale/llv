import { z } from "zod";



export const SignInValidation = z.object({
    email: z.string().email(),
    password: z.string().min(6, {message: "Please enter valid password"})
  })



export const SignUpValidation = z.object({
    title: z.string().min(2, {message: "Please enter valid title"}),
    username: z.string().min(3, {message: "Please enter valid username"}),
    firstname: z.string().min(3, {message: "Please enter valid firstname"}),
    surname: z.string().min(3, {message: "Please enter valid surname"}),
    email: z.string().email(),
    password: z.string().min(6, {message: "Please enter a valid password"}),
    verifyPassword: z.string().min(6, {message: "Please enter a valid password"}),
    organization: z.string(),
    town: z.string().min(3, {message: "Please enter valid town"}),
    country: z.string().min(3, {message: "Please enter valid country"}),
    sex: z.string().min(0, {message: "select a valid gender"}),
    telephone: z.string().min(3, {message: "Please enter a valid telephone"}),
    postalcode: z.string().min(3, {message: "Please enter a valid postalcode"}),

  })




// type UserParams struct {
//     Title        string `json:"title"`
//     Username     string `json:"username"`
//     Firstname    string `json:"firstname"`
//     Surname      string `json:"surname"`
//     Email        string `json:"email"`
//     Organization string `json:"organization"`
//     Town         string `json:"town"`
//     Country      string `json:"country"`
//     Sex          string `json:"sex"`
//     Telephone    string `json:"telephone"`
//     Postalcode   string `json:"postalcode"`
//     Password     string `json:"password"`
//     Isadmin      string `json:"isadmin"`
// }