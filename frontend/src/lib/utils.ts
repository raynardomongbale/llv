import { type ClassValue, clsx } from "clsx"
import { twMerge } from "tailwind-merge"
import Cookies from "js-cookie"
import { useState, useCallback } from "react"
import { CookieAttributes } from "node_modules/@types/js-cookie"


export const baseURL = 'http://localhost:4000'

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs))
}


export default function useCookie(name: string, defaultValue:any) {
  const [value, setValue] = useState(() => {
      const cookie = Cookies.get(name)
      if (cookie) return cookie
      Cookies.set(name, defaultValue)
      return defaultValue
  })

  const updateCookie = useCallback(
      (newValue: string, options: CookieAttributes | undefined) => {
          Cookies.set(name, newValue, options)
          setValue(newValue)
      },
      [name]
  )

  const deleteCookie = useCallback(() => {
      Cookies.remove(name)
      setValue(null)
  }, [name])

  return [value, updateCookie, deleteCookie]
}