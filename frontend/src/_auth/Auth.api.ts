


export interface LoginCredentials {
    username: string
    password: string
}

export interface LoginResponse {
    data: any
    success: string
    token: string
    message: string
}

export interface RegisterCredential {
    email: string
    username: string
    password: string
    organization: string
    repeatPassword: string
}
