import { Outlet, Navigate } from "react-router-dom";
import {useEffect} from "react";
import Nav from "@/_root/Navigations/Nav.tsx";

export const AuthLayout = () => {
const isAuthenticated = false

    useEffect(() => {
        window.scrollTo({
            top: document.body.scrollHeight,
            behavior: 'smooth'
        });
    }, [])

  return (
    <div>
        <div className="h-[100vh]  bg-home bg-no-repeat bg-cover bg-center bg-fixed bg-black">
            <Nav isAuthenticated={false}/>
        </div >



        < div className="bottom-0 left-0 w-full bg-black">
          {
            isAuthenticated ? (
              <Navigate to="/" />
            ): (
            <>
              <section className="flex flex-1 justify-center items-center flex-col py-10">
                <Outlet />
              </section>
            </>
            )
          }
      </div>
    </div>
  )
}
