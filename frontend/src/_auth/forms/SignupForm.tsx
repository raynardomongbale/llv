import {z} from "zod"
import {zodResolver} from "@hookform/resolvers/zod"
import {useForm} from "react-hook-form"
import {
    Form,
    FormControl,
    FormDescription,
    FormField,
    FormItem,
    FormLabel,
    FormMessage,
} from "@/components/ui/form"
import {Input} from "@/components/ui/input"
import {Button} from "@/components/ui/button"
import {SignInValidation, SignUpValidation} from "@/lib/validation"
import {
    Tabs,
    TabsContent,
    TabsList,
    TabsTrigger,
} from "@/components/ui/tabs"
import {useNavigate} from "react-router-dom";
import useSignIn from "react-auth-kit/hooks/useSignIn";
import {useEffect, useState} from "react";
import useAlert from "@/lib/alert.tsx";
import Axios, {AxiosRequestConfig} from "axios";
import {baseURL} from "@/lib/utils.ts";
import {LoginResponse} from "@/_auth/Auth.api.ts";
import Cookies from "js-cookie";
import AlertPopup from "@/lib/alertPop.tsx";
import { CountryDropdown, RegionDropdown } from 'react-country-region-selector';


import {
    Select,
    SelectContent,
    SelectGroup,
    SelectItem,
    SelectTrigger,
    SelectValue,
} from "@/components/ui/select"


const AuthForm = () => {

    const navigate = useNavigate();
    const signIn = useSignIn();
    const [error, errState] = useState<string>('')
    const {setAlert} = useAlert();

    const loggingForm = useForm<z.infer<typeof SignInValidation>>({
        resolver: zodResolver(SignInValidation),
        defaultValues: {
            email: "",
            password: "",
        },
    })

    const registerForm = useForm<z.infer<typeof SignUpValidation>>({
        resolver: zodResolver(SignUpValidation),
        defaultValues: {
            title: "",
            username: "",
            firstname: "",
            surname: "",
            email: "",
            password: "",
            verifyPassword: "",
            organization: "",
            town: "",
            country: "",
            sex: "",
            telephone: "",
            postalcode: "",
        }
    })

    const [orgs, setOrgs] = useState([])

    useEffect(() => {
        const url = baseURL + '/organizations'

        fetch(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${Cookies.get("_auth")}`
            }
        }).then(response => response.json())
            .then((data) => {
                if (data.success === false) {
                    console.log(data.message)
                } else {
                    setOrgs(data.data)
                }
            })
            .catch(error => {
                console.log(error)
                console.log(error.response.data.message);
            });
    }, [])

    const onLogin = async (values: z.infer<typeof SignInValidation>) => {
        errState("");

        const requestConfig: AxiosRequestConfig = {
            method: 'post',
            url: baseURL + '/login',
            params: values,
            validateStatus: function (status) {
                return status >= 200 && status < 400
            }
        }

        try {
            const resp = await Axios.request<LoginResponse>(requestConfig);

            signIn({
                auth: {
                    token: resp.data.token,
                    type: 'Bearer',
                },
            });
            console.log(resp.data)
            Cookies.set("username", resp.data.data.username)
            Cookies.set("firstname", resp.data.data.firstname)
            Cookies.set("isAdmin", resp.data.data.isadmin)
            setAlert(resp.data.message, "success")
            navigate("/")
        } catch (err) {
            if (err && err instanceof Axios) {
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-expect-error
                errState(err.response.data.message);
            } else if (err && err instanceof Error) {
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-expect-error
                errState(err.response.data.message);
            }
            console.error(error)
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-expect-error
            setAlert(err?.response.data.message, "error")
        }
    }


    const onRegister = async (values: z.infer<typeof SignUpValidation>) => {
        event?.preventDefault
        errState("");

        const url = baseURL + '/register'

        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
            body: JSON.stringify(values),
        }).then(response => response.json())
            .then((data) => {
                if (data.success === false) {
                    setAlert(data.message, "error")
                } else {
                    signIn({
                        auth: {
                            token: data.token,
                            type: 'Bearer',
                        },
                    });
                    Cookies.set("username", data.data.username)
                    Cookies.set("firstname", data.data.firstname)
                    Cookies.set("isAdmin", data.data.isadmin)
                    setAlert(data.message, "success")
                    navigate("/")
                }
            })
            .catch(error => {
                console.log(error)
                setAlert(error?.response.data.message, "error")
            });
    }

    return (
        <div className="flex-center flex-col text-white space-y-6">
            <AlertPopup />
            <a href="/" className='flex items-center space-x-3 rtl:space-x-reverse'>
                <img src="../../public/images/logo-2.jpeg" className="h-8 rounded-full " alt=" Logo"/>
                <span className='flex text-gold'><p className=''>LeicesterLawn</p>Ventures.</span>
            </a>


            <Tabs defaultValue="sign-in" className="w-[400px]">
                <TabsList className="grid w-full grid-cols-2 bg-gold">
                    <TabsTrigger value="sign-in">Sign In</TabsTrigger>
                    <TabsTrigger value="sign-up">Register</TabsTrigger>
                </TabsList>
                <TabsContent value="sign-in">
                    <Form {...loggingForm}>
                        <div className="sm:w-420 flex-center flex-col ">
                            <FormDescription> Sign into your account</FormDescription>
                            <form onSubmit={loggingForm.handleSubmit(onLogin)}
                                  className="flex flex-col gap-5 w-full mt-4">
                                <FormField
                                    control={loggingForm.control}
                                    name="email"
                                    render={({field}) => (
                                        <FormItem>
                                            <FormLabel>Email</FormLabel>
                                            <FormControl className="text-ox-blood">
                                                <Input placeholder="email" {...field} />
                                            </FormControl>
                                            <FormMessage/>
                                        </FormItem>
                                    )}
                                />
                                <FormField
                                    control={loggingForm.control}
                                    name="password"
                                    render={({field}) => (
                                        <FormItem>
                                            <FormLabel>Password</FormLabel>
                                            <FormControl>
                                                <Input className="text-ox-blood" type={"password"} placeholder="password" {...field} />
                                            </FormControl>
                                            <FormDescription>
                                                <a></a>
                                            </FormDescription>
                                            <FormMessage/>
                                        </FormItem>
                                    )}
                                />
                                <Button type="submit">Submit</Button>
                            </form>
                        </div>
                    </Form>
                </TabsContent>
                <TabsContent value="sign-up">
                    <Form {...registerForm}>
                        <div className="sm:w-420 flex-center flex-col text-white">
                            <FormDescription> Open an account</FormDescription>
                            <form onSubmit={registerForm.handleSubmit(onRegister)}
                                  className="flex flex-col gap-5 w-full mt-4">


                                <FormField control={registerForm.control}
                                           name="title"
                                           render={({field}) => (
                                               <FormItem>
                                                   <FormLabel>Title</FormLabel>
                                                   <Select onValueChange={field.onChange} defaultValue={field.value}>
                                                       <FormControl>
                                                           <SelectTrigger className="w-[100px] text-black font-sans ">
                                                               <SelectValue placeholder="Mr"/>
                                                           </SelectTrigger>
                                                       </FormControl>

                                                       <SelectContent>
                                                           <SelectGroup>
                                                               <SelectItem value="Mr" >Mr</SelectItem>
                                                               <SelectItem value="Mrs">Mrs</SelectItem>
                                                               <SelectItem value="Miss">Miss</SelectItem>
                                                           </SelectGroup>
                                                       </SelectContent>
                                                   </Select>


                                                   <FormMessage/>
                                               </FormItem>
                                           )}
                                />
                                <FormField
                                    control={registerForm.control}
                                    name="username"
                                    render={({field}) => (
                                        <FormItem>
                                            <FormLabel>Username</FormLabel>
                                            <FormControl>
                                                <Input className="text-ox-blood" placeholder="username" {...field} />
                                            </FormControl>
                                            <FormMessage/>
                                        </FormItem>
                                    )}
                                />
                                <FormField
                                    control={registerForm.control}
                                    name="firstname"
                                    render={({field}) => (
                                        <FormItem>
                                            <FormLabel>First name</FormLabel>
                                            <FormControl>
                                                <Input className="text-ox-blood" placeholder="First name" {...field} />
                                            </FormControl>
                                            <FormMessage/>
                                        </FormItem>
                                    )}
                                />
                                <FormField
                                    control={registerForm.control}
                                    name="surname"
                                    render={({field}) => (
                                        <FormItem>
                                            <FormLabel>Last name</FormLabel>
                                            <FormControl>
                                                <Input className="text-ox-blood" placeholder="Last name" {...field} />
                                            </FormControl>
                                            <FormMessage/>
                                        </FormItem>
                                    )}
                                />
                                <FormField
                                    control={registerForm.control}
                                    name="email"
                                    render={({field}) => (
                                        <FormItem>
                                            <FormLabel>Email</FormLabel>
                                            <FormControl>
                                                <Input className="text-ox-blood" placeholder="email" {...field} />
                                            </FormControl>
                                            <FormMessage/>
                                        </FormItem>
                                    )}
                                />
                                <FormField control={registerForm.control}
                                           name="password"
                                           render={({field}) => (
                                               <FormItem>
                                                   <FormLabel>Password</FormLabel>
                                                   <FormControl>
                                                       <Input className="text-ox-blood"
                                                              type={"password"}
                                                              placeholder="password" {...field} />
                                                   </FormControl>
                                                   <FormMessage/>
                                               </FormItem>
                                           )}
                                />
                                <FormField
                                    control={registerForm.control}
                                    name="verifyPassword"
                                    render={({field}) => (
                                        <FormItem>
                                            <FormLabel>Verify Password</FormLabel>
                                            <FormControl>
                                                <Input className="text-ox-blood"
                                                       type={"password"}
                                                       placeholder="verify password" {...field} />
                                            </FormControl>
                                            <FormMessage/>
                                        </FormItem>
                                    )}
                                />
                                <FormField control={registerForm.control}
                                           name="organization"
                                           render={({field}) => (
                                               <FormItem>
                                                   <FormLabel className='text-gold'>Organizer</FormLabel>
                                                   <Select onValueChange={field.onChange} defaultValue={field.value}>
                                                       <FormControl>
                                                           <SelectTrigger className="w-[250px] text-black font-sans ">
                                                               <SelectValue placeholder="select Org."/>
                                                           </SelectTrigger>
                                                       </FormControl>

                                                       <SelectContent>
                                                           <SelectGroup>

                                                               {orgs.map(({id, name}) => (
                                                                   <SelectItem key={id} value={id}>{name}</SelectItem>
                                                               ))}
                                                               <SelectItem  key="0" value="0">Others</SelectItem>
                                                           </SelectGroup>
                                                       </SelectContent>
                                                   </Select>


                                                   <FormMessage/>
                                               </FormItem>
                                           )}
                                />



                                <FormField
                                    control={registerForm.control}
                                    name="country"

                                    render={({ field }) => (
                                        <FormItem className="w-[250px]  font-sans ">
                                            <FormLabel >Country </FormLabel>

                                            <CountryDropdown
                                                classes={"w-[250px] h-[40px] text-black font-sans round"}
                                                value={field.value}
                                                onChange={field.onChange} />
                                        </FormItem>
                                    )}
                                />


                                <FormField
                                    control={registerForm.control}
                                    name="town"
                                    render={({ field }) => (
                                        <FormItem className=" text-sm">
                                            <FormLabel >Region </FormLabel>

                                            <RegionDropdown
                                                classes={"text-black font-sans "}
                                                country={registerForm.getValues('country')}
                                                value={field.value}
                                                onChange={field.onChange} />
                                        </FormItem>
                                    )}
                                />



                                <FormField control={registerForm.control}
                                           name="sex"
                                           render={({field}) => (
                                               <FormItem>
                                                   <FormLabel>Gender</FormLabel>
                                                   <Select onValueChange={field.onChange} defaultValue={field.value}>
                                                       <FormControl>
                                                           <SelectTrigger className="w-[180px] text-black font-sans ">
                                                               <SelectValue placeholder="Select a Gender"/>
                                                           </SelectTrigger>
                                                       </FormControl>

                                                       <SelectContent>
                                                           <SelectGroup>
                                                               <SelectItem value="M" >Male</SelectItem>
                                                               <SelectItem value="F">Female</SelectItem>
                                                               <SelectItem value="O">Others</SelectItem>
                                                           </SelectGroup>
                                                       </SelectContent>
                                                   </Select>


                                                   <FormMessage/>
                                               </FormItem>
                                           )}
                                />
                                <FormField control={registerForm.control}
                                           name="telephone"
                                           render={({field}) => (
                                               <FormItem>
                                                   <FormLabel>Telephone</FormLabel>
                                                   <FormControl>
                                                       <Input className="text-ox-blood"
                                                              placeholder="telephone" {...field} />
                                                   </FormControl>
                                                   <FormMessage/>
                                               </FormItem>
                                           )}
                                />
                                <FormField control={registerForm.control}
                                           name="postalcode"
                                           render={({field}) => (
                                               <FormItem>
                                                   <FormLabel>Postal code</FormLabel>
                                                   <FormControl>
                                                       <Input className="text-ox-blood"
                                                              placeholder="postal code" {...field} />
                                                   </FormControl>
                                                   <FormMessage/>
                                               </FormItem>
                                           )}
                                />
                                <Button type="submit">Submit</Button>
                            </form>
                        </div>
                    </Form>
                </TabsContent>
            </Tabs>
        </div>


    )
}

export default AuthForm