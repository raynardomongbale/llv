
import NavigationMenuBar from './NavigationMenu';
import useSignOut from 'react-auth-kit/hooks/useSignOut';

import { Separator } from "@/components/ui/separator"




interface Props {
    isAuthenticated: boolean;
}

const Nav = ({ isAuthenticated }: Props) => {
    const signOut = useSignOut();

    const visibleOnAuth = !isAuthenticated ? "" : "hidden";
    const hiddenOnAuth = !isAuthenticated ? "hidden" : "";


    const Logout = () => {
        signOut();
        window.location.reload();
    }



    return (
        <div >
            <nav className='text-ox-blood bg-gold font-serif'>

                <div className='flex flex-wrap items-center justify-between max-w-screen-xl mx-auto p-4'>

                    <a href="/" className='flex items-center space-x-2 rtl:space-x-reverse'>
                        <img src="../../public/images/logo-2.jpeg" className="h-8 rounded-full " alt=" Logo" />
                        <span className='flex text-ox-blood'><p className='text-white'>LeicesterLawn</p>Ventures.</span>
                    </a>

                    <div className="flex items-center md:order-2 space-x-1 md:space-x-2 rtl:space-x-reverse">
                        <div className={`${visibleOnAuth} xs:hidden`}>
                            <a href="/sign-in" className=" text-gray-800 dark:text-white hover:bg-gray-50 focus:ring-4 focus:ring-gray-300 font-medium rounded-lg text-sm px-4 py-2 md:px-5 md:py-2.5 dark:hover:bg-gray-700 focus:outline-none dark:focus:ring-gray-800">Login</a>
                            <a href="/sign-in" className=" text-gray-800 dark:text-white hover:bg-gray-50 focus:ring-4 focus:ring-gray-300 font-medium rounded-lg text-sm px-4 py-2 md:px-5 md:py-2.5 dark:hover:bg-gray-700 focus:outline-none dark:focus:ring-gray-800">Register</a>
                        </div>

                        <div className={`${hiddenOnAuth}`}>
                            <button onClick={Logout} className=" text-gray-800 bg-white dark:text-white hover:bg-gray-50 focus:ring-4 focus:ring-gray-300 font-medium rounded-lg text-sm px-4 py-2 md:px-5 md:py-2.5 dark:hover:bg-gray-700 focus:outline-none dark:focus:ring-gray-800">Logout</button>
                        </div>

                    </div>
                    <NavigationMenuBar />
                </div>
            </nav>
            <Separator className=" bg-gold" />

        </div>
    )
}

export default Nav
