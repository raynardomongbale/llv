"use client"
import {
  Menubar,
  MenubarContent,
  MenubarItem,
  MenubarMenu,
  MenubarSeparator,
  MenubarShortcut,
  MenubarSub,
  MenubarSubContent,
  MenubarSubTrigger,
  MenubarTrigger,
} from "@/components/ui/menubar"
import { useNavigate } from 'react-router-dom';
import Cookies from "js-cookie"


interface Props {
  isAuthenticated: boolean;
}




// const profiles = ["Profile 1"]



const MenuBarNav = ({ isAuthenticated }: Props) => {
  const authed = isAuthenticated ? "" : "hidden";
  // const [profile, setProfile] = useState(profiles[0])
  const navigate = useNavigate();
  const isAdmin = () => {
    if (!isAuthenticated) {
      return false
    } else {
      return Cookies.get("isAdmin")
    }
  }
  const AdminSection = () => {

    if (isAdmin()) {
      return (
        <MenubarSub>
          <MenubarSubTrigger >Admin</MenubarSubTrigger>
          <MenubarSubContent>
            <MenubarItem onClick={() => navigate('/admin/courses/create')}>Add Courses</MenubarItem>
            <MenubarSeparator />
            <MenubarItem onClick={() => navigate('/admin/course-request')}>View Course Requests</MenubarItem>
          </MenubarSubContent>
        </MenubarSub >
      )
    }
  }


  return (
    <div className={`fixed bottom-0 left-0 w-full ${authed}`}>
      <Menubar className='flex justify-center space-x-4'>
        <MenubarMenu>
          <MenubarTrigger >My Account</MenubarTrigger>
          <MenubarContent>
            <MenubarItem onClick={() => navigate('/user/profile')}>
              My Profile
            </MenubarItem>
            <MenubarItem onClick={() => navigate('/user/organizations')}>
              Groups and Organizations
            </MenubarItem>
            <MenubarSeparator />
            <MenubarSub >
              <MenubarSubTrigger>Inbox</MenubarSubTrigger>
              <MenubarSubContent>
                <MenubarItem disabled>Messages</MenubarItem>
                <MenubarItem onClick={() => window.open("https://mail.google.com/mail/u/0")}>My Email</MenubarItem>
                <MenubarItem disabled>Notes</MenubarItem>
              </MenubarSubContent>
            </MenubarSub>
          </MenubarContent>
        </MenubarMenu>
        <MenubarMenu>
          <MenubarTrigger>Programs/Courses</MenubarTrigger>
          <MenubarContent>
            <MenubarSub>
              <MenubarSubTrigger disabled>Programs (coming soon.) </MenubarSubTrigger>
              <MenubarSubContent>
                <MenubarItem>Search for Program (Using program code) </MenubarItem>
                <MenubarSeparator />
                <MenubarItem>Find programs...</MenubarItem>
                <MenubarItem>My programs</MenubarItem>
                <MenubarItem>
                  Print programs ..... <MenubarShortcut>⌘P</MenubarShortcut>
                </MenubarItem>
              </MenubarSubContent>
              <MenubarSeparator />
            </MenubarSub>
            <MenubarSeparator />
            <MenubarSub>
              <MenubarSubTrigger>Courses</MenubarSubTrigger>
              <MenubarSubContent>
                <MenubarItem>Search for Course (Using program code) </MenubarItem>
                <MenubarSeparator />
                <MenubarItem onClick={() => navigate('/user/courses')}>My Courses</MenubarItem>
                <MenubarItem>
                  Print Courses ... <MenubarShortcut>⌘P</MenubarShortcut>
                </MenubarItem>
              </MenubarSubContent>
              <MenubarSeparator />
            </MenubarSub>
            <AdminSection />
          </MenubarContent>
        </MenubarMenu>
        {/*<MenubarMenu>*/}
        {/*  <MenubarTrigger>Profiles</MenubarTrigger>*/}
        {/*  <MenubarContent>*/}
        {/*    /!*<MenubarRadioGroup value={profile} onValueChange={setProfile}>*!/*/}
        {/*    /!*  {profiles.map((item) => (*!/*/}
        {/*    /!*    <MenubarRadioItem value={item} >*!/*/}
        {/*    /!*      {item}*!/*/}
        {/*    /!*    </MenubarRadioItem>*!/*/}
        {/*    /!*  ))}*!/*/}
        {/*    /!*</MenubarRadioGroup>*!/*/}
        {/*    /!*  <MenubarSeparator />*!/*/}
        {/*    <MenubarItem >Edit...</MenubarItem>*/}
        {/*    <MenubarSeparator />*/}
        {/*    <MenubarItem >Add Profile...</MenubarItem>*/}

        {/*      <MenubarItem>*/}
        {/*          Print profile ...*/}
        {/*      </MenubarItem>*/}
        {/*      <MenubarSeparator />*/}
        {/*      <MenubarItem>*/}
        {/*          Delete Profile ...*/}
        {/*      </MenubarItem>*/}
        {/*  </MenubarContent>*/}
        {/*</MenubarMenu>*/}
      </Menubar>
    </div>
  )
}

export default MenuBarNav;