import { RxCrossCircled } from "react-icons/rx";


interface Props {
    showNav: boolean;
    closeNav: () => void;
    loggedIn: boolean;
}

const MobileNav = ({showNav, closeNav, loggedIn}: Props) => {
    const navStyle = showNav?"translate-x-0":"translate-x-[-100%]";
    const signInStyle = loggedIn?"lg:hidden":"";
    const signOutStyle =!loggedIn?"lg:hidden":"";

  return (
    <div className={`fixed ${navStyle} right-0 transistion-all duration-500 left-0 bottom-0 h-[100vh] bg-[#000000e0] z-[1002]`}>
        <button>
            <span >
                Close
            </span>
            <span>
                <RxCrossCircled onClick={closeNav} className="absolute top-[2rem] right-[2rem] w-[2rem] h-[2rem] text-white"/>
            </span>
        </button>
     
        
        {/* nav Div */}
        <div className={`bg-gold ${navStyle} transistion-all duration-500 delay-200 flex flex-col items-center justify-center w-[20%] h-[100%]`}>
        
        {/* nav links */}
        <ul className="space-y-10">
            <li className="text-[35px] font-medium hover:text-yellow-400">
                <a href="/" >
                    Search
                </a>
            </li>
            <li className="text-[35px] font-medium hover:text-yellow-400">
                <a href="/" >
                    Profile
                </a>
            </li>
            <li className="text-[35px] font-medium hover:text-yellow-400">
                <a href="/" >
                    Courses
                </a>
            </li>
            <li className={`text-[35px] font-medium ${signOutStyle} hover:text-yellow-400`}>
                <a href="/" >
                    Logout
                </a>
            </li>
            <li className={`text-[35px] font-medium ${signInStyle} hover:text-yellow-400`}>
                <a href="/" >
                    Sign-in
                </a>
            </li>
            <li className={`text-[35px] font-medium ${signInStyle} hover:text-yellow-400`}>
                <a href="/" >
                    Register
                </a>
            </li>
        </ul>
        </div>

    </div>
  )
}

export default MobileNav