"use client"

import { useState } from 'react'
import Nav from './Nav'
import MobileNav from './MobileNav'
import useIsAuthenticated from 'react-auth-kit/hooks/useIsAuthenticated'


const ResponsiveNav = () => {
   
const authenticated = useIsAuthenticated()
    const [showNav, setShowNav] = useState(false)
    const closeNavHandler=() => setShowNav(false)


  return (
    <div className="h-[72vh] bg-hero bg-no-repeat bg-cover bg-center bg-fixed dark:bg-black">
        <Nav isAuthenticated={useIsAuthenticated()}/>
        <MobileNav showNav={showNav} closeNav={closeNavHandler} loggedIn={authenticated}/>
    </div>
  )
}

export default ResponsiveNav