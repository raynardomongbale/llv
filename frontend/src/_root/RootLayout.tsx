import { Outlet } from 'react-router-dom'




const RootLayout = () => {
  return (
    <>
      <section className="flex flex-col ">
        <Outlet />
      </section>
    </>
  )
}

export default RootLayout