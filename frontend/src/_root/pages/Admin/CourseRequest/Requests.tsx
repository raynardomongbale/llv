import { baseURL } from '@/lib/utils'
import BookingDataPage from './Table/BookingDataPage'


const Request = () => {
    const url = baseURL + '/verified/admin/bookings/pending'
    return (
        <div>
            <div className="h-[100vh]  bg-course-background-2 bg-no-repeat bg-cover bg-center bg-fixed bg-black">
                <div className="h-[20vh]"></div>
                <div >
                    {/* <DataTable url={url} authedRequest={false} /> */}
                    <BookingDataPage url={url} />
                </div>

            </div>
        </div>
    )
}

export default Request