import {ColumnDef} from "@tanstack/react-table"

import {Button} from "@/components/ui/button"
import {
    DropdownMenu,
    DropdownMenuContent,
    DropdownMenuItem,
    DropdownMenuLabel,
    DropdownMenuSeparator,
    DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu"
import {DotsHorizontalIcon} from "@radix-ui/react-icons"
import {Checkbox} from "@/components/ui/checkbox"
import {Booking} from "@/lib/models"
import {handleApproval} from "./Booking"



export const columns: ColumnDef<Booking>[] = [

    {
        id: "select",
        header: ({table}) => (
            <Checkbox
                checked={
                    table.getIsAllPageRowsSelected() ||
                    (table.getIsSomePageRowsSelected() && "indeterminate")
                }
                onCheckedChange={(value) => table.toggleAllPageRowsSelected(!!value)}
                aria-label="Select all"
            />
        ),
        cell: ({row}) => (
            <Checkbox
                checked={row.getIsSelected()}
                onCheckedChange={(value) => row.toggleSelected(!!value)}
                aria-label="Select row"
            />
        ),
        enableSorting: false,
        enableHiding: false,
    },
    {
        accessorKey: "bookingId",
        header: "BookingID",
        cell: ({row}) => (
            <div className="capitalize">{row.getValue("bookingId")}</div>
        ),
    },
    {
        accessorKey: "name",
        header: "CourseName",
        cell: ({row}) => (
            <div className="capitalize">{row.getValue("name")}</div>
        ),
    },
    {
        accessorKey: "title",
        header: "Fullname",
        cell: ({row}) => (
            <div
                className="capitalize">{row.getValue("title")} {row.getValue("firstname")} {row.getValue("surname")}</div>

        ),
    },
    {
        accessorKey: "firstname",
    },
    {
        accessorKey: "surname",
    },
    {
        accessorKey: "size",
        header: "Course Size",
        cell: ({row}) => (
            <div className="capitalize">{row.getValue("size")}</div>
        ),
    },
    {
        accessorKey: "state",
        header: "Booking State",
        cell: ({row}) => (
            <div className="capitalize">{row.getValue("state")}</div>
        ),
    },
    {
        id: "actions",
        enableHiding: false,
        cell: ({row}) => {
            const booking = row.original



            return (
                <DropdownMenu>
                    <DropdownMenuTrigger asChild>
                        <Button variant="ghost" className="h-8 w-8 p-0">
                            <span className="sr-only">Open menu</span>
                            <DotsHorizontalIcon className="h-4 w-4"/>
                        </Button>
                    </DropdownMenuTrigger>
                    <DropdownMenuContent align="end">
                        <DropdownMenuLabel>Actions</DropdownMenuLabel>
                        <DropdownMenuItem
                            onClick={() => navigator.clipboard.writeText(String(booking.bookingId))}
                        >
                            Copy Booking ID
                        </DropdownMenuItem>
                        <DropdownMenuSeparator/>
                        <DropdownMenuItem >View
                            <a href={`/booking?query=${booking.bookingId}`}>Booking details</a></DropdownMenuItem>
                        <DropdownMenuItem
                            onClick={() => {
                                handleApproval(booking.bookingId)
                            }}
                        >Approve Booking
                        </DropdownMenuItem>
                    </DropdownMenuContent>
                </DropdownMenu>
            )
        },
    },
]
