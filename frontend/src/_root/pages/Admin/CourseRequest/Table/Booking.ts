import {baseURL} from "@/lib/utils";


export const handleApproval = async (bookingId: number) => {
    const requestApprovalUrl = baseURL + "/verified/admin/bookings/approve"


    fetch(requestApprovalUrl, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            // 'Authorization': `Bearer ${Cookies.get("_auth")}`
        },
        body: JSON.stringify({bookingId: bookingId})
    }).then(response => {

        console.log(response)
        if (!response.ok) {
            throw new Error(response.statusText)
        }
        window.alert("Booking Approved")
    }).catch(error => {
        window.alert("Error Approving Booking")
        return {success: false, message: error.message}
    })
};


