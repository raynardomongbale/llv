import { baseURL } from '@/lib/utils'
import { useEffect, useState } from 'react'
import { useLocation, useNavigate } from 'react-router-dom'
import queryString from 'query-string';
import { Separator } from "@/components/ui/separator"
import { Button } from "@/components/ui/button"
import useIsAuthenticated from 'react-auth-kit/hooks/useIsAuthenticated';
import { LoginResponse } from '@/_auth/Auth.api';
import Axios, { AxiosRequestConfig } from 'axios'
import { CourseType } from '@/lib/models.ts';



function EnrollmentStatus(props: { state: any; }) {
    const state = props.state
    return (
        <div>
            (state === "Unknown") ? "" : (
            <div className="flex h-5 items-center space-x-4 text-sm text-white">
                <Separator orientation="vertical" />
                <div>Enrollment Status: {state}</div>
                <Separator orientation="vertical" />
            </div>
            <Separator className="my-4" />
            );
        </div>
    )
}

const Course = () => {

    const url = baseURL + '/courses/course/'
    const buildUrl = (query: any) => {
        return `${url}${query}`
    }
    const [err, errState] = useState('')

    const [data, setData] = useState<CourseType>()

    const location = useLocation();
    const  query  = queryString.parse(location.search);

    const fetchData = async (query: string) => {
        const queryUrl = buildUrl(query)
        try {
            const response = await fetch(queryUrl);
            if (!response.ok) {
                throw new Error('Failed to fetch data');
            }
            const jsonifiedResponse = await response.json();
            const course = jsonifiedResponse.data
            setData(course)
        } catch (error) {
            // errState(error)
            console.error('Error:', err);
        }
    };

    useEffect(() => {
        if (query.toString() == "") return
        if (query.toString() == undefined) return
        fetchData(query.toString());
    }, [query ]);

    const navigate = useNavigate()
    const isAuthenticated = useIsAuthenticated();


    const onSubmit = async () => {


        const requestConfig: AxiosRequestConfig = {
            method: 'post',
            url: baseURL + '/bookings/enroll',
            validateStatus: function (status) {
                return status >= 200 && status < 400
            }
        }

        try {
            const resp = await Axios.request<LoginResponse>(requestConfig);
            console.log(resp)
            navigate("/user/courses")
        } catch (error) {
            if (error && error instanceof Axios)
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                { // @ts-expect-error
                    errState(error.response?.data.message);
                }
            else if (error && error instanceof Error)
                errState(error.message);
            window.alert("Error: " + err);
            console.log("Error:", err)
        }
    }

    const enrollCourse = async () => {
        !isAuthenticated ? navigate("/sign-in") : onSubmit
    }



    // const [enrollmentState, setEnrollmentState] = useState(0);



    // const fetchEnrollment = async () => {
    //
    //     const requestConfig: AxiosRequestConfig = {
    //         method: 'post',
    //         url: baseURL + '/verified/enrollment',
    //         validateStatus: function (status) {
    //             return status >= 200 && status < 400
    //         }
    //     }
    //
    //     try {
    //         const resp = await Axios.request<LoginResponse>(requestConfig);
    //         console.log(resp)
    //
    //     } catch (err: any) {
    //         if (err && err instanceof Axios)
    //             errState(err.response?.data.message);
    //         else if (err && err instanceof Error)
    //             errState(err.message);
    //         window.alert("Error: " + "err.response.data.message");
    //         console.log("Error:", err)
    //     }
    //
    // }

    // const getEnrolledState = async () => {
    //     isAuthenticated ? fetchEnrollment : console.log("not authenticated")
    // }

    const enddate = new Date("2024-04-20T00:01:01Z").toLocaleDateString('en-US', { dateStyle: "full" });;

    const startdate = new Date("2024-04-20T00:01:01Z").toLocaleDateString('en-US', { dateStyle: "full" });;

    const finalRegistrationDate = new Date("2024-04-20T00:01:01Z").toLocaleDateString('en-US', { dateStyle: "full" });;



    return (
        <div className="flex h-screen ">
            {/* Left section for background image */}
            <div className="hidden md:block md:w-1/2 bg-cover bg-center" style={{ backgroundImage: "url('../public/images/classroom.jpeg')" }}></div>

            <div className="w-full md:w-1/2 p-8 flex items-center justify-center flex-col bg-black
  
             ">
                <div >
                    <div className="space-y-1 text-white">
                        <h2 className="text-sm font-medium leading-none">{data?.name}</h2>
                        <p className="text-sm text-muted-foreground ">
                            {data?.description}
                        </p>
                    </div>
                    <Separator className="my-4" />
                    <div className="flex h-5 items-center space-x-4 text-sm text-white">
                        <Separator orientation="vertical" />
                        <div>Size: {data?.size}</div>
                        <Separator orientation="vertical" />
                        <div>No Enrolled: {data?.enrolled}</div>
                        <Separator orientation="vertical" />
                    </div>
                    <Separator className="my-4" />
                    <div className="flex h-5 items-center space-x-4 text-sm text-white">
                        <Separator orientation="vertical" />
                        <div>Period : </div>
                        <div>{startdate}</div>
                        <p>-</p>
                        <div>{enddate}</div>
                        <Separator orientation="vertical" />
                    </div>
                    <Separator className="my-4" />
                    <div className="flex h-5 items-center space-x-4 text-sm text-white">
                        <Separator orientation="vertical" />
                        <div>{data?.country}</div>
                        <Separator orientation="vertical" />
                        <div>{data?.town}</div>
                        <Separator orientation="vertical" />
                    </div>
                    <Separator className="my-4" />
                    <div className="flex h-5 items-center space-x-4 text-sm text-white">
                        <Separator orientation="vertical" />
                        <div>Final Date of Registration: {finalRegistrationDate}</div>
                        <Separator orientation="vertical" />
                    </div>
                    <Separator className="my-4" />
                    <EnrollmentStatus state={data?.state} />
                </div>
                <div className=" ">

                    <Button
                        variant="outline"
                        size="sm"
                        onClick={enrollCourse}
                    >
                        Enroll
                    </Button>
                </div>

            </div>
        </div>
    )
}

export default Course