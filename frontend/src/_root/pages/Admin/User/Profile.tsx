import { baseURL } from '@/lib/utils'
import { useEffect, useState } from 'react'
import useIsAuthenticated from 'react-auth-kit/hooks/useIsAuthenticated'
import Cookies from "js-cookie"


import {
    Card,
    CardContent,
    CardFooter,
    CardHeader,
    CardTitle,
} from "@/components/ui/card"
import Nav from '@/_root/Navigations/Nav'
import { UserType } from '@/lib/models'





const Profile = () => {

    const url = baseURL + '/verified/profile'


    const [data, setData] = useState<UserType>()
    const [err, errState] = useState('')

    const isAuthenticated = useIsAuthenticated();


    const fetchData = async () => {
        try {
            const response = await fetch(url, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${Cookies.get("_auth")}`
                }
            });
            console.log(response)
            if (!response.ok) {
                throw new Error('Failed to fetch data');
            }
            const jsonifiedResponse = await response.json();
            const course = jsonifiedResponse.data
            setData(course)
            console.log(data)
        } catch (error) {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-expect-error
            errState(error.message)
            console.log(err)
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="h-[100vh]  bg-bikes bg-no-repeat bg-cover bg-center bg-fixed bg-black">

            <Nav isAuthenticated={isAuthenticated} />

            <div className=" p-40 w-full justify-center items-center">
                <Card className="w-[550px] bg-black text-white">
                    <CardHeader>
                        <CardTitle>Profile</CardTitle>
                    </CardHeader>
                    <CardContent className="grid gap-4">
                        <div className=" flex items-center space-x-4 rounded-md border p-4">

                            <div className="flex-1 space-y-1">
                                <p className="text-sm font-medium leading-none">
                                    {data?.title} {data?.firstname} {data?.surname}
                                </p>
                                <p className="text-sm text-muted-foreground">
                                    username: {data?.username}
                                </p>
                            </div>

                        </div>
                        <div>

                            <div

                                className="mb-4 grid grid-cols-[25px_1fr] items-start pb-4 last:mb-0 last:pb-0"
                            >
                                <span className="flex h-2 w-2 translate-y-1 rounded-full bg-sky-500" />
                                <div className="space-y-1">
                                    <p className="text-sm text-muted-foreground">
                                        Phone: {data?.telephone}
                                    </p>
                                    <p className="text-sm text-muted-foreground">
                                        Email: {data?.email}
                                    </p>
                                    <p className="text-sm text-muted-foreground">
                                        Town: {data?.town}
                                    </p>
                                    <p className="text-sm text-muted-foreground">
                                        Country: {data?.country}
                                    </p>
                                    <p className="text-sm text-muted-foreground">
                                        Postal Code: {data?.postalcode}
                                    </p>
                                    <p className="text-sm text-muted-foreground">
                                        Country: {data?.country}
                                    </p>
                                </div>
                            </div>

                        </div>
                    </CardContent>
                    <CardFooter>

                    </CardFooter>

                </Card>

            </div>

        </div>
    )
}

export default Profile