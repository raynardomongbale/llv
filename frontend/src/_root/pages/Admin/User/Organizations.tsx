import { baseURL } from '@/lib/utils'
import { useEffect, useState } from 'react'
import useIsAuthenticated from 'react-auth-kit/hooks/useIsAuthenticated'
import Cookies from "js-cookie"


import {
    Card,
    CardContent,
    CardHeader,
    CardTitle,
} from "@/components/ui/card"
import Nav from '@/_root/Navigations/Nav'




export type Organization = {
    id: string
    name: string
    description: string
    website: string
    email: string
    telephone: string
    country: string
    town: string
    postalcode: string
}

type organizations = Organization[];
const Organizations = () => {

    const url = baseURL + '/verified/organizations'

    const [err, errState] = useState('')



    // const signOut = useSignOut();
    const isAuthenticated = useIsAuthenticated();

    // const visibleOnAuth = !isAuthenticated ? "" : "hidden";
    // const hiddenOnAuth = !isAuthenticated ? "hidden" : "";

    //
    // const Logout = () => {
    //     signOut();
    //     window.location.reload();
    // }


    const [orgs, setOrganizations] = useState<organizations>([])

    const fetchData = async () => {
        try {
            const response = await fetch(url, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${Cookies.get("_auth")}`
                }
            });

            if (!response.ok) {
                throw new Error('Failed to fetch data');
            }
            const jsonifiedResponse = await response.json();

            let org = jsonifiedResponse.data
            setOrganizations(org)
        } catch (error: any) {
            errState(error.message)
            console.log(err)
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="h-[100vh]  bg-bikes bg-no-repeat bg-cover bg-center bg-fixed bg-black">
            <Nav isAuthenticated={isAuthenticated}/>


            {orgs.map(org =>
            (<div>


                <div className=" flex flex-wrap justify-between max-w-screen-xl mx-auto p-4">
                    <Card className="w-[550px] bg-black text-white">
                        <CardHeader>
                            <CardTitle>{org?.name}</CardTitle>
                        </CardHeader>
                        <CardContent className="grid gap-4">
                            <div className=" flex items-center space-x-4 rounded-md border p-4">

                                <div className="flex-1 space-y-1">
                                    <p className="text-sm text-muted-foreground">
                                        description: @{org?.description}
                                    </p>


                                    <p className="text-sm text-muted-foreground">
                                        website: {org?.website}
                                    </p>


                                    <p className="text-sm text-muted-foreground">
                                        email: {org?.email}
                                    </p>


                                    <p className="text-sm text-muted-foreground">
                                        telephone: {org?.telephone}
                                    </p>

                                    <p className="text-sm text-muted-foreground">
                                        town: {org?.town}
                                    </p>
                                    <p className="text-sm text-muted-foreground">
                                        country: {org?.country}
                                    </p>
                                    <p className="text-sm text-muted-foreground">
                                        postalcode: {org?.postalcode}
                                    </p>

                                </div>
                            </div>
                        </CardContent>
                    </Card>

                </div>




            </div>
            ))}

        </div >
    )
}


export default Organizations