"use strict";
import {useEffect, useState} from 'react';
import { useForm } from "react-hook-form"
import {
    Form,
    FormControl,
    FormDescription,
    FormField,
    FormItem,
    FormLabel, FormMessage,
} from "@/components/ui/form"
import { Input } from "@/components/ui/input"
import { Button } from "@/components/ui/button"
import { Calendar } from "@/components/ui/calendar"

import {
    Popover,
    PopoverContent,
    PopoverTrigger,
} from "@/components/ui/popover"
import Cookies from "js-cookie"



import { CalendarIcon } from "@radix-ui/react-icons"
import { baseURL, cn } from "@/lib/utils"

import { useNavigate } from 'react-router-dom';
import { CountryDropdown, RegionDropdown } from 'react-country-region-selector';
import { addDays, format } from "date-fns"
import { DateRange } from "react-day-picker"
import {Select, SelectContent, SelectGroup, SelectItem, SelectTrigger, SelectValue} from "@/components/ui/select.tsx";
import useAlert from "@/lib/alert.tsx";

const AddCourseForm = () => {
    const [course, setCourse] = useState({
        name: '',
        instructor: '',
        startdate: new Date(),
        enddate: new Date(),
        final_registration_date: new Date(),
        description: '',
        size: '',
        town: '',
        country: '',
        price: '',
        currency: '',
        organizationId: ''
    });

    const {setAlert} = useAlert();
    const [orgs, setOrgs] = useState([])

    useEffect(() => {
        const url = baseURL + '/organizations'

        fetch(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${Cookies.get("_auth")}`
            }
        }).then(response => response.json())
            .then((data) => {
                if (data.success === false) {
                    console.log(data.message)
                } else {
                    console.log(data)
                    setOrgs(data.data)
                }
            })
            .catch(error => {
                console.log(error)
                console.log(error.response.data.message);
            });
    }, [])


    const form = useForm()
    
    const navigate = useNavigate()

    const [date, setDate] = useState<DateRange | undefined>({
        from: new Date(),
        to: addDays(new Date(), 20),
    })


    const handleChange = (e:any) => {
        const { name, value } = e.target;
        setCourse(prevCourse => ({
            ...prevCourse,
            [name]: value
        }));
    };
    const handleChangeVariable = (name: string, value: string) => {
        setCourse(prevCourse => ({
            ...prevCourse,
            [name]: value
        }));
    };

    const handleChangeTime = (name: string, value: any | undefined) => {
        setCourse(prevCourse => ({
            ...prevCourse,
            [name]: value
        }));
    };

    const onSubmit = async () => {
        event?.preventDefault


        const url = baseURL + '/verified/admin/course/create'
    console.log(JSON.stringify(course))
        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${Cookies.get("_auth")}`
            },
            body: JSON.stringify(course),
        }).then(response => response.json())
            .then((data) => {
                if (data.success === false) {
                    console.log(data.message)
                    setAlert(data.message, data.message)
                } else {
                navigate("/courses/search")
                }
            })
            .catch(error => {
                console.log(error.response.data.message)
                window.alert(error.response.data.message);
            });
    }
    return (
        <div className="flex h-screen ">
            {/* Left section for background image */}
            <div className="hidden md:block md:w-1/2 bg-cover bg-center" style={{ backgroundImage: "url('../../public/images/buildings.jpeg')" }}></div>

            {/* Right section for form */}
            <div className="w-full md:w-1/2 p-8 flex items-center justify-center flex-col bg-course-background">
                {/* Form component goes here */}

                <a href="/" className='flex items-center space-x-3 rtl:space-x-reverse'>
                    <img src="../../public/images/logo-2.jpeg" className="h-8 rounded-full " alt=" Logo" />
                    <span className='flex text-gold'><p className=''>LeicesterLawn</p>Ventures.</span>
                </a>
                {/* <h2 className="text-2xl font-bold mb-4 text-white">Add New Course</h2> */}
                <Form {...form}>



                    <form onSubmit={form.handleSubmit(onSubmit)} className="flex flex-col gap-5 w-full mt-4">
                        <FormField
                            control={form.control}
                            name="name"
                            render={({ field }) => (
                                <FormItem>
                                    <FormDescription>
                                        Enter course details
                                    </FormDescription>

                                    <FormLabel className='text-gold'>Title</FormLabel>
                                    <FormControl>
                                        <Input placeholder="course title" {...field} onChange={handleChange} />
                                    </FormControl>
                                </FormItem>
                            )}
                        />

                        <FormField control={form.control}
                                   name="organizationId"
                                   render={({field}) => (
                                       <FormItem>
                                           <FormLabel className='text-gold'>Organizer</FormLabel>
                                           <Select onValueChange={field.onChange} defaultValue={field.value}>
                                               <FormControl>
                                                   <SelectTrigger className="w-[250px] text-black font-sans ">
                                                       <SelectValue placeholder="select Org."/>
                                                   </SelectTrigger>
                                               </FormControl>

                                               <SelectContent>
                                                   <SelectGroup>

                                                       {orgs.map(({id, name}) => (
                                                           <SelectItem key={id} value={id}>{name}</SelectItem>
                                                       ))}
                                                       <SelectItem  key="0" value="0">Others</SelectItem>
                                                   </SelectGroup>
                                               </SelectContent>
                                           </Select>


                                           <FormMessage/>
                                       </FormItem>
                                   )}
                        />
                        <FormItem className='flex-col flex'>
                            <FormLabel className='text-gold'>Start/End Date</FormLabel>
                            <div className={cn("grid gap-2")}>
                                <Popover>
                                    <PopoverTrigger asChild>
                                        <Button
                                            id="date"
                                            variant={"outline"}
                                            className={cn(
                                                "w-[300px] justify-start text-left font-normal",
                                                !date && "text-muted-foreground"
                                            )}
                                        >
                                            <CalendarIcon className="mr-2 h-4 w-4" />
                                            {course.startdate ? (
                                                course.enddate ? (
                                                    <>
                                                        {format(course.startdate, "LLL dd, y")} -{" "}
                                                        {format(course.enddate, "LLL dd, y")}
                                                    </>
                                                ) : (
                                                    format(course.startdate, "LLL dd, y")
                                                )
                                            ) : (
                                                <span>Pick a date</span>
                                            )}
                                        </Button>
                                    </PopoverTrigger>
                                    <PopoverContent className="w-auto p-0" align="start">
                                        <Calendar
                                            initialFocus
                                            mode="range"
                                            defaultMonth={course.startdate}
                                            selected={date}
                                            onSelect= {(val) => {
                                                    handleChangeTime("startdate", val?.from);
                                                    handleChangeTime("enddate", val?.to);
                                                    setDate(val);
                                                }}
                                            numberOfMonths={2}
                                        />
                                    </PopoverContent>
                                </Popover>
                            </div>

                        </FormItem>


                        <FormItem className='flex-col flex'>
                            <FormLabel className='text-gold'>Final Date of Registration</FormLabel>
                            <Popover>
                                <PopoverTrigger asChild>
                                    <Button
                                        variant={"outline"}
                                        className={cn(
                                            "w-[280px] justify-start text-left font-normal",
                                            !course.final_registration_date && "text-muted-foreground"
                                        )}
                                    >
                                        <CalendarIcon className="mr-2 h-4 w-4" />
                                        {course.final_registration_date ? format(course.final_registration_date, "PPP") : <span>Final Date of Reg.</span>}
                                    </Button>
                                </PopoverTrigger>
                                <PopoverContent className="w-auto p-0">
                                    <Calendar
                                        mode="single"

                                        selected={course.final_registration_date}
                                        onSelect={(val) => handleChangeTime("final_registration_date", val)}
                                        initialFocus
                                    />
                                </PopoverContent>
                            </Popover>
                        </FormItem>

                        <FormField
                            control={form.control}
                            name="description"
                            render={({ field }) => (
                                <FormItem>
                                    <FormLabel className='text-gold'>Description</FormLabel>
                                    <FormControl>
                                        <Input placeholder="Course Description" {...field} onChange={handleChange} />
                                    </FormControl>
                                </FormItem>
                            )}
                        />
                        <FormField
                            control={form.control}
                            name="price"
                            render={({ field }) => (
                                <FormItem>
                                    <FormLabel className='text-gold'>Cost</FormLabel>
                                    <FormControl>
                                        <Input placeholder="10,000" {...field} onChange={handleChange} />
                                    </FormControl>
                                </FormItem>
                            )}
                        />

                        <FormField control={form.control}
                                   name="currency"
                                   render={({field}) => (
                                       <FormItem>
                                           <FormLabel className='text-gold'>Currency</FormLabel>
                                           <Select onValueChange={field.onChange} defaultValue={field.value}>
                                               <FormControl>
                                                   <SelectTrigger className="w-[180px] text-black font-sans ">
                                                       <SelectValue placeholder="Select currency"/>
                                                   </SelectTrigger>
                                               </FormControl>

                                               <SelectContent>
                                                   <SelectGroup>
                                                       <SelectItem value="#" >Naira</SelectItem>
                                                       <SelectItem value="$">Dollars</SelectItem>
                                                       <SelectItem value="P">Pounds</SelectItem>
                                                       <SelectItem value="E">Euro</SelectItem>
                                                   </SelectGroup>
                                               </SelectContent>
                                           </Select>


                                           <FormMessage/>
                                       </FormItem>
                                   )}
                        />

                        <FormField
                            control={form.control}
                            name="size"
                            render={({ field }) => (
                                <FormItem>
                                    <FormLabel className='text-gold'>Size</FormLabel>
                                    <FormControl>
                                        <Input placeholder="Course Size" {...field} onChange={handleChange} />
                                    </FormControl>
                                </FormItem>
                            )}
                        />

                        <FormField
                            control={form.control}
                            name="country"
                            render={({ field }) => (
                                <FormItem>
                                    <FormLabel className='text-gold'>Country </FormLabel>

                                    <CountryDropdown
                                        value={course.country}
                                        onChange={(val) => {handleChangeVariable('country', val)
                                            field.onChange(val)}
                                        }
                                    />
                                </FormItem>
                            )}
                        />


                        <FormField
                            control={form.control}
                            name="town"
                            render={({ field }) => (
                                <FormItem>
                                    <FormLabel className='text-gold'>Region </FormLabel>

                                    <RegionDropdown
                                        country={course.country}
                                        value={course.town}
                                        // onChange={field.onChange}
                                        onChange={(val) => {handleChangeVariable('town', val)
                                            field.onChange(val)}
                                        }
                                    />
                                </FormItem>
                            )}
                        />
                        <Button className='bg-ox-blood text-white' onSubmit={onSubmit} type="submit">Submit</Button>
                    </form>
                </Form>
            </div>
        </div>

    );
};

export default AddCourseForm;