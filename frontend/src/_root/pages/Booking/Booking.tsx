import { baseURL } from '@/lib/utils'
import { useEffect, useState } from 'react'
import { useLocation, useNavigate } from 'react-router-dom'
import queryString from 'query-string';
import { Separator } from "@/components/ui/separator"
import { Button } from "@/components/ui/button"
import useIsAuthenticated from 'react-auth-kit/hooks/useIsAuthenticated';
import Cookies from "js-cookie"
import { CourseType } from '@/lib/models';





export const Booking = () => {
    const navigate = useNavigate()
    const isAuthenticated = useIsAuthenticated();

    const url = baseURL + '/verified/bookings/list/'
    const buildUrl = (query: any) => {
        return `${url}${query}`
    }


    const [data, setData] = useState<CourseType>()

    const location = useLocation();
    const { query } = queryString.parse(location.search);

    const fetchData = async (query: string) => {
        const queryUrl = buildUrl(query)
        console.log(queryUrl)
        try {
            const response = await fetch(queryUrl, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${Cookies.get("_auth")}`
                }
            });
            console.log(response)
            if (!response.ok) {
                throw new Error(response.statusText);
            }
            const jsonifiedResponse = await response.json();
            const course = jsonifiedResponse.data
            setData(course)
        } catch (error) {

            console.error(error);
        }
    };


    useEffect(() => {
        if (query == "") return
        if (query == undefined) return
        fetchData(query?.toString());
    }, [query]);


    function EnrollmentStatus(props: { state: any; }) {
        return (
            <div>
                (!isAuthenticated ) ? (
                <div className="flex h-5 items-center space-x-4 text-sm text-white">
                    <Separator orientation="vertical" />
                    <div className='text-green-500'>Enrollment Status: {props.state}</div>
                    <Separator orientation="vertical" />
                </div>
                <Separator className="my-4" />
                );
                (enrollmentStatus  === "Not Enrolled") ? (
                <div className=" ">
                    <Button
                        variant="outline"
                        size="sm"
                        onClick={() => {
                            !isAuthenticated ? navigate("/sign-in") : onSubmit()
                        }}
                    >
                        Enroll
                    </Button>
                </div>
                ):(

                );

            </div>
        )
    }


    const onSubmit = async () => {
        const url = baseURL + '/verified/admin/bookings/approve'


        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${Cookies.get("_auth")}`
            },
            body: JSON.stringify({ courseId: data?.id })
        }).then(response => response.json())
            .then((data) => {
                if (data.success === false) {
                    window.alert(data.message)
                    window.location.reload()
                } else {
                    navigate("/user/courses")
                }
            })
            .catch(error => {
                window.alert(error);
            });
    }


    return (
        <div className="flex h-screen ">
            {/* Left section for background image */}
            <div className="hidden md:block md:w-1/2 bg-cover bg-center" style={{ backgroundImage: "url('../public/images/classroom.jpeg')" }}></div>

            <div className="w-full md:w-1/2 p-8 flex items-center justify-center flex-col bg-black

             ">
                <div >
                    <div className="space-y-1 text-white">
                        <h2 className="text-sm font-medium leading-none">{data?.name}</h2>
                        <p className="text-sm text-muted-foreground ">
                            {data?.description}
                        </p>
                    </div>
                    <Separator className="my-4" />
                    <div className="flex h-5 items-center space-x-4 text-sm text-white">
                        <Separator orientation="vertical" />
                        <div>Size: {data?.size}</div>
                        <Separator orientation="vertical" />
                        <div>No Enrolled: {data?.enrolled}</div>
                        <Separator orientation="vertical" />
                    </div>
                    <Separator className="my-4" />
                    <div className="flex h-5 items-center space-x-4 text-sm text-white">
                        <Separator orientation="vertical" />
                        <div>Period : </div>
                        <div>{data?.startdate}</div>
                        <p>-</p>
                        <div>{data?.enddate}</div>
                        <Separator orientation="vertical" />
                    </div>
                    <Separator className="my-4" />
                    <div className="flex h-5 items-center space-x-4 text-sm text-white">
                        <Separator orientation="vertical" />
                        <div>{data?.country}</div>
                        <Separator orientation="vertical" />
                        <div>{data?.town}</div>
                        <Separator orientation="vertical" />
                    </div>
                    <Separator className="my-4" />
                    <div className="flex h-5 items-center space-x-4 text-sm text-white">
                        <Separator orientation="vertical" />
                        <div>Final Date of Registration: {data?.final_registration_date}</div>
                        <Separator orientation="vertical" />
                    </div>
                    <Separator className="my-4" />
                    <EnrollmentStatus state={data?.state} />
                </div>
            </div>
        </div>
    )
}
