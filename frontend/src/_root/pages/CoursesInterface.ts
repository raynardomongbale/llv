



export interface Course {
    Courseid: number;
    Name: {
        String: string;
        Valid: boolean;
    };
    Size: number;
    Enrolled: number;
    Description: {
        String: string;
        Valid: boolean;
    };
    Startdate: {
        Time: string;
        Valid: boolean;
    };
    Enddate: {
        Time: string;
        Valid: boolean;
    };
    Finalregistrationdate: {
        Time: string;
        Valid: boolean;
    };
    Town: {
        String: string;
        Valid: boolean;
    };
    Country: {
        String: string;
        Valid: boolean;
    };
}