import React, {useEffect, useState} from 'react'
import Nav from '../Navigations/Nav'
import Gallery from './Home/Gallery'
import useIsAuthenticated from 'react-auth-kit/hooks/useIsAuthenticated'
import MenuBarNav from '../Navigations/MenuBarNav'
import { Skeleton } from '@/components/ui/skeleton'
import { useNavigate } from 'react-router-dom'
import Cookies from "js-cookie"
import AlertPopup from "@/lib/alertPop.tsx";

const Home = () => {
    const isAuthenticated = useIsAuthenticated()

    const navigate = useNavigate()

    const hiddenOnAuth = !isAuthenticated ? "hidden" : "";

    const [inputValue, setInputValue] = useState<string>('');



    useEffect(() => {
        window.scrollTo({
            top: document.body.scrollHeight,
            behavior: 'smooth'
        });

        setTimeout(() => {
            window.scrollTo({
                top: document.body.scrollTop,
                behavior: 'smooth'
            });
        }, 1000);

    }, [])

    const FirstNameValue = () => {
        const cookie = Cookies.get('firstname');
        if (cookie) return cookie;
        return ""
    }

    const handleSubmit = async (e: { preventDefault: () => void }) => {
        e.preventDefault();
        navigate(`/courses/search?query=${inputValue}`);
    };

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setInputValue(e.target.value);
    };


    return (
        <>
            <div className="h-[100vh]  bg-home bg-no-repeat bg-cover bg-center bg-fixed bg-black text-ox-blood">

                <Nav isAuthenticated={isAuthenticated}/>
                <AlertPopup/>

                <div className=" p-40 w-full justify-center items-center">
                    <form onSubmit={handleSubmit} className="flex items-center max-w-lg mx-auto">
                        <div className="relative w-full">
                            <div className="absolute inset-y-0 start-0 flex items-center ps-3 pointer-events-none">
                                <svg className="w-4 h-4 text-gray-500 dark:text-gray-400" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 21 21">
                                    <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M11.15 5.6h.01m3.337 1.913h.01m-6.979 0h.01M5.541 11h.01M15 15h2.706a1.957 1.957 0 0 0 1.883-1.325A9 9 0 1 0 2.043 11.89 9.1 9.1 0 0 0 7.2 19.1a8.62 8.62 0 0 0 3.769.9A2.013 2.013 0 0 0 13 18v-.857A2.034 2.034 0 0 1 15 15Z" />
                                </svg>
                            </div>
                            <input type="text" onChange={handleChange} id="voice-search" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full ps-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Search Courses..." required />
                            {/* <ul>
                            {searchResults.map((result, index) => (
                                <li key={index}>{result.name}</li> 
                            ))}
                        </ul> */}
                            <button type="button" onClick={handleSubmit} className="absolute inset-y-0 end-0 flex items-center pe-3">
                                <svg className="w-4 h-4 text-gray-500 dark:text-gray-400 hover:text-gray-900 dark:hover:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 16 20">
                                    <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M15 7v3a5.006 5.006 0 0 1-5 5H6a5.006 5.006 0 0 1-5-5V7m7 9v3m-3 0h6M7 1h2a3 3 0 0 1 3 3v5a3 3 0 0 1-3 3H7a3 3 0 0 1-3-3V4a3 3 0 0 1 3-3Z" />
                                </svg>
                            </button>
                        </div>
                        <button type="submit" className="inline-flex items-center py-2.5 px-3 ms-2 text-sm font-medium text-white bg-black rounded-lg border border-yellow-500  hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                            <svg className="w-4 h-4 me-2" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20">
                                <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z" />
                            </svg>Search
                        </button>
                    </form>
                </div>


                <div className={`h-[30vh] mx-auto p-20 space-y-3 ${hiddenOnAuth}`}>
                    <div className="flex items-center space-x-4">
                        {/* <Skeleton className="h-12 w-12 rounded-full" /> */}
                    </div>
                    <div className='flex flex-col'>
                        <Skeleton className="p-5 h-[100px] w-[250px] bg-white rounded-xl" >
                            <p className='text-sm text-black leading-tight text-muted-foreground'>Welcome {FirstNameValue()}, <br /> Glad to have you back...</p>
                        </Skeleton>
                    </div>

                </div>

                <MenuBarNav isAuthenticated={isAuthenticated} />


            </div>

            {/* <MobileNav showNav={showNav} closeNav={closeNavHandler} loggedIn={isAuthenticated} /> */}
            <Gallery />
        </>
    )
}

export default Home