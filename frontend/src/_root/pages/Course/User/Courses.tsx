import { DataTable } from '../Search/DataPage'
import { baseURL } from '@/lib/utils'
import useIsAuthenticated from 'react-auth-kit/hooks/useIsAuthenticated'
import Nav from '@/_root/Navigations/Nav'


const Courses = () => {
  const url = baseURL + '/verified/bookings'



  return (
    <div>
        <div className="h-[100vh]  bg-course-background-2 bg-no-repeat bg-cover bg-center bg-fixed bg-black">
            <Nav isAuthenticated={useIsAuthenticated()}/>
            <div >
                <DataTable url={url} authedRequest={true}/>
            </div>

        </div>
    </div>
)
}

export default Courses