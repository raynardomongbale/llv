import { baseURL } from '@/lib/utils';
import { DataTable } from './DataPage';
import Nav from '@/_root/Navigations/Nav';
import useIsAuthenticated from 'react-auth-kit/hooks/useIsAuthenticated';

const Search = () => {

    const url = baseURL + '/courses/list'

    return (
        <div>
            <div className="h-[100vh]  bg-course-background-2 bg-no-repeat bg-cover bg-center bg-fixed bg-black">
            <Nav isAuthenticated={useIsAuthenticated()}/>
                <div >
                    <DataTable url={url} authedRequest={false}/>
                </div>

            </div>
        </div>
    )
}

export default Search;
