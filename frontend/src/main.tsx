import ReactDOM  from "react-dom/client";
import {BrowserRouter} from "react-router-dom";
import App from "./App";
import AuthProvider from "react-auth-kit"
import createStore from 'react-auth-kit/createStore';
import {AlertProvider} from "@/lib/alertContext.tsx";


const store = createStore({
    authName:'_auth',
    authType:'cookie',
    cookieDomain: window.location.hostname,
    cookieSecure: window.location.protocol === 'https:',
  });

ReactDOM.createRoot(document.getElementById('root')!).render(
    <AuthProvider store={store}>
        <BrowserRouter>
            <AlertProvider><App /></AlertProvider>
        </BrowserRouter>
    </AuthProvider>
)

