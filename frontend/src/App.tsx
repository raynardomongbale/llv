import {Routes, Route} from "react-router-dom";
import "./globals.css";
import {Home} from "./_root/pages";
import {AuthLayout} from "./_auth/AuthLayout";
import RootLayout from "./_root/RootLayout";
import AuthOutlet from '@auth-kit/react-router/AuthOutlet'
import Profile from "./_root/pages/Admin/User/Profile";
import AddCourseForm from "./_root/pages/Admin/AddCoursePage";

import Search from "./_root/pages/Course/Search/Search";
import Course from "./_root/pages/Course/Course";
import Courses from "./_root/pages/Course/User/Courses";
import Organizations from "./_root/pages/Admin/User/Organizations";
import Request from "./_root/pages/Admin/CourseRequest/Requests";
import AuthForm from "./_auth/forms/SignupForm";
import {Booking} from "@/_root/pages/Booking/Booking.tsx";


const App = () => {
    return (
        <main className="">
            <Routes>
                {/* { Public routes} */}
                <Route element={<RootLayout/>}>
                    <Route path="/" element={<Home/>}/>
                    <Route path="/course" element={<Course/>}/>
                    <Route path="/courses/search" element={<Search/>}/>
                </Route>

                {/* Authenticating Routes */}
                <Route element={<AuthLayout/>}>
                    <Route path="/sign-in" element={<AuthForm/>}/>
                </Route>

                {/* private routes */}
                <Route element={<AuthOutlet fallbackPath='/sign-in'/>}>
                    <Route path='/user/profile' element={<Profile/>}/>
                    <Route path='/user/organizations' element={<Organizations/>}/>
                    <Route path='/user/courses' element={<Courses/>}/>
                    {/* Admin Routes */}
                    <Route path='/admin/courses/create' element={<AddCourseForm/>}/>
                    <Route path='/admin/course-request' element={<Request/>}/>
                    <Route path='/booking' element={<Booking/>}/>
                </Route>

            </Routes>
        </main>

    )
}

export default App