# Telling to use Docker's golang ready image
FROM golang
# Name and Email of the author
MAINTAINER Raynard Omongbale <raynardomongbale@gmail.com>
# Create app folder
RUN mkdir /go/src/app/
WORKDIR /go/src/app/

COPY ./backend/* /go/src/app/

# Download all the dependencies
RUN go mod download -x

# Install compile daemon for hot reloading
RUN go install -mod=mod github.com/githubnemo/CompileDaemon

EXPOSE 4000
# Run the app binarry file
ENTRYPOINT CompileDaemon --build="go build main.go" --command=./main