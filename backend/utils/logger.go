package utils

import (
	"fmt"
	"io"
	"log/slog"
	"os"
	"strings"
	"time"
)

const (
	// MaxLogsContentLength max logs content length. Limit set to not hit logz.io 32700 content lengths hard limit.
	MaxLogsContentLength = 30000
	// LogsTruncatedMessage truncated message for any logs that are too long for content max length.
	LogsTruncatedMessage = "... [truncated]"
)

// LoggableRequest is expected to be a request which defines loggable fields about itself
// in a key-pair slice, e.g. []interface{"method", "list", "project_guid", "my-prj-id"}
type LoggableRequest interface {
	LogFields() []interface{}
}

// CommonLogger constructs a logger which functions the same way across all apps.
// It is configured as such:
// - Debug level logging
// - Logs to configured writer
// - Includes source field
// - Includes app field
func CommonLogger(appName string) *slog.Logger {
	return CommonLoggerWithOptions(appName, os.Stdout, slog.LevelDebug, true)
}

// CommonLoggerWithOptions constructs a logger with specific options.
func CommonLoggerWithOptions(appName string, w io.Writer, level slog.Level, json bool) *slog.Logger {
	opts := &slog.HandlerOptions{
		AddSource:   true,
		Level:       level,
		ReplaceAttr: replaceAttrFunc,
	}

	var logger *slog.Logger

	if json {
		logger = slog.New(slog.NewJSONHandler(w, opts))
	} else {
		logger = slog.New(slog.NewTextHandler(w, opts))
	}

	logger = logger.With(
		"app", appName,
	)

	slog.SetDefault(logger)
	return logger
}

// LogRequest logs the request time and request-specific log fields to the provided
// logger at either Debug or Error log level, depending on whether or not err points to
// a non-nil error. Designed to be used via a defer (hence the error pointer, since we
// don't want to evaluate the value of the error until the defer func is called).
func LogRequest(logger *slog.Logger, begin time.Time, err *error, req LoggableRequest) {
	kv := make([]interface{}, 0)

	if req != nil {
		kv = append(kv, req.LogFields()...)
	}

	kv = append(kv, "took", time.Since(begin).Milliseconds())
	kv = append(kv, "took_string", time.Since(begin).String())

	if *err != nil {
		kv = append(kv, "err", *err)
		logger.Error("request error", kv...)
		return
	}

	// Recover from panic in order to log out so that it's caught by Kibana
	// Then throw the panic again in order to print the full stack trace in the kube logs and respond with a 503
	if pErr := recover(); pErr != nil {
		kv = append(kv, "details", fmt.Sprintf("%v", pErr))
		logger.Error("panic occurred (see kube logs for more details)", kv...)
		// Noticed a bug in that sometimes the panic will appear simultaneously to log line, cutting it in half
		// and preventing Kibana from picking it up so this sleep should ensure they are separated
		time.Sleep(10 * time.Millisecond)
		panic(pErr)
	}

	logger.Debug("successful request", kv...)
}

// replaceAttrFunc replaces select attribute fields in an incoming log record to suit logging standards we need.
func replaceAttrFunc(_ []string, a slog.Attr) slog.Attr {
	switch a.Key {
	case slog.MessageKey:
		// Move 'msg' to 'message' to preserve backwards-compatibility.
		a.Key = "message"

	case slog.LevelKey:
		// Make 'level' key lowercase to preserve backwards-compatibility.
		a.Value = slog.StringValue(strings.ToLower(a.Value.String()))

	case slog.SourceKey:
		// Cut the source file to a smaller path.
		s, ok := a.Value.Any().(*slog.Source)
		// If the source is not a slog.Source, then we don't want to modify it.
		if !ok {
			break
		}
		v := strings.Split(s.File, "/")
		idx := len(v) - 2
		if idx < 0 {
			idx = 0
		}

		// Move 'source' to 'caller' to preserve backwards compatibility.
		a.Key = "caller"
		a.Value = slog.StringValue(strings.Join(v[idx:], "/") + fmt.Sprintf(":%d", s.Line))
	default:
		// Replace dots in key with underscores to help logz.io indexer / field mapper.
		a.Key = strings.Replace(a.Key, ".", "_", -1)
	}

	// Trim content length due to logz.io 32700 character limit.
	// Error: MAX_FIELD_KEY_SIZE or INVALID_FIELD_VALUE_LENGTH where
	// attribute "Exceeded the maximum of 32700 characters per field"

	if len(a.Value.String()) > MaxLogsContentLength {
		a.Value = slog.StringValue(a.Value.String()[:MaxLogsContentLength] + LogsTruncatedMessage)
	}

	return a
}
