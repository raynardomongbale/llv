package utils

import (
	"fmt"
	"log/slog"
	"os"
	"time"

	"github.com/sirupsen/logrus"
)

type (
	SlogShim struct {
		l       *slog.Logger
		appName string
	}
)

// NewSlogShim returns a shim to other packages, using the golang structured logger.
func NewSlogShim(l *slog.Logger) *SlogShim {
	return &SlogShim{
		l: l,
	}
}

// NewSlogShimByAppName returns a shim to other packages, using the golang structured logger providing an application name.
// This should be used by a global logger to get the correct "app" key on the logs so that they are transmitteed to logz.io
// correctly.
func NewSlogShimByAppName(appName string) *SlogShim {
	return &SlogShim{
		appName: appName,
		l:       CommonLogger(appName),
	}
}

func (s SlogShim) Debugf(format string, args ...interface{}) {
	s.l.Debug(fmt.Sprintf(format, args...))
}

func (s SlogShim) Infof(format string, args ...interface{}) {
	s.l.Info(fmt.Sprintf(format, args...))
}

func (s SlogShim) Warnf(format string, args ...interface{}) {
	s.l.Warn(fmt.Sprintf(format, args...))
}

func (s SlogShim) Errorf(format string, args ...interface{}) {
	s.l.Error(fmt.Sprintf(format, args...))
}

func (s SlogShim) Print(v ...interface{}) {
	s.l.Info(fmt.Sprintf("%v", v))
}

func (s SlogShim) WithField(key string, value interface{}) *logrus.Entry {
	// Shouldn't be used, provided for interoperability.
	return s.logrusEntryShim().WithField(key, value)
}

func (s SlogShim) WithFields(fields logrus.Fields) *logrus.Entry {
	// Shouldn't be used, provided for interoperability.
	return s.logrusEntryShim().WithFields(fields)
}

func (s SlogShim) WithError(err error) *logrus.Entry {
	// Shouldn't be used, provided for interoperability.
	return s.logrusEntryShim().WithError(err)
}

func (s SlogShim) Printf(format string, args ...interface{}) {
	s.l.Info(fmt.Sprintf(format, args...))
}

func (s SlogShim) Warningf(format string, args ...interface{}) {
	s.l.Warn(fmt.Sprintf(format, args...))
}

func (s SlogShim) Fatalf(format string, args ...interface{}) {
	s.l.Error(fmt.Sprintf(format, args...))
	os.Exit(1)
}

func (s SlogShim) Panicf(format string, args ...interface{}) {
	s.l.Error(fmt.Sprintf(format, args...))
	panic(fmt.Sprintf(format, args...))
}

func (s SlogShim) Debug(args ...interface{}) {
	s.l.Debug(fmt.Sprintf("%v", args))
}

func (s SlogShim) Info(args ...interface{}) {
	s.l.Info(fmt.Sprintf("%v", args))
}

func (s SlogShim) Warn(args ...interface{}) {
	s.l.Warn(fmt.Sprintf("%v", args))
}

func (s SlogShim) Warning(args ...interface{}) {
	s.l.Warn(fmt.Sprintf("%v", args))
}

func (s SlogShim) Error(args ...interface{}) {
	s.l.Error(fmt.Sprintf("%v", args))
}

func (s SlogShim) Fatal(args ...interface{}) {
	s.l.Error(fmt.Sprintf("%v", args))
	os.Exit(1)
}

func (s SlogShim) Panic(args ...interface{}) {
	s.l.Error(fmt.Sprintf("%v", args))
	panic(fmt.Sprintf("%v", args))
}

func (s SlogShim) Debugln(args ...interface{}) {
	s.l.Debug(fmt.Sprintf("%v", args))
}

func (s SlogShim) Infoln(args ...interface{}) {
	s.l.Info(fmt.Sprintf("%v", args))
}

func (s SlogShim) Println(args ...interface{}) {
	s.l.Info(fmt.Sprintf("%v", args))
}

func (s SlogShim) Warnln(args ...interface{}) {
	s.l.Warn(fmt.Sprintf("%v", args))
}

func (s SlogShim) Warningln(args ...interface{}) {
	s.l.Warn(fmt.Sprintf("%v", args))
}

func (s SlogShim) Errorln(args ...interface{}) {
	s.l.Error(fmt.Sprintf("%v", args))
}

func (s SlogShim) Fatalln(args ...interface{}) {
	s.l.Error(fmt.Sprintf("%v", args))
	os.Exit(1)
}

func (s SlogShim) Panicln(args ...interface{}) {
	s.l.Error(fmt.Sprintf("%v", args))
	panic(fmt.Sprintf("%v", args))
}

// logrusEntryShim returns a logrus entry for shim-purposes which makes the output look the same as the common logger.
func (s SlogShim) logrusEntryShim() *logrus.Entry {
	lg := logrus.New()
	lg.Formatter = &logrus.JSONFormatter{
		TimestampFormat: time.RFC3339Nano,
	}
	return logrus.NewEntry(lg).WithFields(logrus.Fields{
		"app": s.appName,
	})
}
