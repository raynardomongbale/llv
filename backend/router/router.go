package router

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/raynard2/auth"
	"github.com/raynard2/controllers"
	"github.com/raynard2/controllers/admin/bankAccount"
	booking3 "github.com/raynard2/controllers/admin/booking"
	"github.com/raynard2/controllers/admin/organization"
	"github.com/raynard2/controllers/booking"
	"github.com/raynard2/controllers/course"
	"github.com/raynard2/controllers/users"
)

func NewApp() *echo.Echo {

	engine := echo.New()
	engine.Use(middleware.Logger())
	engine.Use(middleware.Recover())
	engine.Use(cors)

	engine.Debug = true
	engine.Use(middleware.Recover())
	engine.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "[ECHO] - ${time_rfc3339} |${status}| ${latency_human} | ${host} | ${method} ${uri}\n",
	}))

	engine.GET("/", controllers.Home)
	engine.GET("/about", func(c echo.Context) error {
		return c.String(http.StatusOK, "About")
	})
	engine.GET("/courses/list", course.List)
	engine.GET("/courses/course", course.GetByName)
	engine.GET("/courses/course/:id", course.Get)
	engine.GET("/organizations", organization.ListOrganization)

	engine.POST("/login", users.Login)
	engine.POST("/register", users.Register)

	authedUser := engine.Group("/verified")
	authedUser.Use(middleware.JWTWithConfig(middleware.JWTConfig{
		Claims:                  &auth.Claims{},
		SigningKey:              []byte(auth.GetJWTSecret()),
		TokenLookup:             "header:Authorization",
		ErrorHandlerWithContext: auth.JWTErrorChecker,
	}))

	authedUser.GET("/profile", users.GetUser)

	authedUser.GET("/bookings", booking.List)
	authedUser.POST("/logout", users.Logout)

	courses := authedUser.Group("/bookings")
	courses.POST("/enroll", booking.Enroll)
	courses.GET("/list", booking.List)
	courses.GET("/list/:id", booking.Get)

	authedAdmin := authedUser.Group("/admin")
	authedAdmin.Use(auth.IsAdmin)
	authedAdmin.POST("/course/create", course.Create)
	authedAdmin.POST("/bookings/approve", booking3.ApproveEnrollment)
	authedAdmin.GET("/bookings/pending", booking3.PendingApprovalList)

	//authedAdmin.POST("/bookings/approve", booking3.ApproveEnrollment)
	//authedAdmin.GET("/bookings/pending", booking3.PendingApprovalList)
	//Organizations
	authedAdmin.POST("/organizations/create", organization.CreateOrganization)
	authedAdmin.GET("/organizations/:name", organization.GetOrganization)
	authedAdmin.GET("/organizations", organization.ListOrganization)
	//Bank Accounts
	authedAdmin.POST("/bank/create", bankAccount.CreateBankAccount)
	authedAdmin.GET("/bank/list", bankAccount.ListBankAccount)
	//authedAdmin.GET("/bank/:id", organization.GetBankAccount)

	return engine
}

func cors(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		c.Response().Header().Set("Access-Control-Allow-Origin", "*")
		c.Response().Header().Set("Access-Control-Allow-Credentials", "true")
		c.Response().Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With, Token, Response-Type")
		c.Response().Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, PATCH, DELETE")
		c.Response().Header().Set("Access-Control-Expose-Headers", "Content-Disposition")
		if c.Request().Method == "OPTIONS" {
			return c.NoContent(http.StatusOK)
		}
		return next(c)
	}
}
