package tools

import (
	"time"

	_ "github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx"
)

// DBConfig is the configuration
type DBConfig struct {
	Driver          string        `env:"DB_DRIVER"`
	DSN             string        `env:"DB_DSN"`
	ConnMaxIdleTime time.Duration `env:"DB_CONN_MAX_IDLE_TIME"` // ConnMaxIdleTime sets the maximum time a connection will stay idle for if not zero.
	ConnMaxLifetime time.Duration `env:"DB_CONN_MAX_LIFETIME"`  // ConnMaxLifetime sets the maximum lifetime of a connection if not zero.
	MaxIdleConns    int           `env:"DB_MAX_IDLE_CONNS"`     // MaxIdleConns sets the maximum number of idle connections if not zero.
	MaxOpenConns    int           `env:"DB_MAX_OPEN_CONNS"`     // MaxOpenConns sets the maximum number of open connections if not zero.
}

// Open opens a database specified by the passed configuration
func Open(cfg DBConfig) (*sqlx.DB, error) {
	db, err := sqlx.Open(cfg.Driver, cfg.DSN)
	if err != nil {
		return nil, err
	}

	if cfg.ConnMaxIdleTime != 0 {
		db.SetConnMaxIdleTime(cfg.ConnMaxIdleTime)
	}
	if cfg.ConnMaxLifetime != 0 {
		db.SetConnMaxLifetime(cfg.ConnMaxLifetime)
	} else {
		// Prevent Timeout errors by setting the ConnMaxLifetime to 1/2 of the default wait_timeout
		db.SetConnMaxLifetime(time.Second * 14400)
	}
	if cfg.MaxOpenConns != 0 {
		db.SetMaxOpenConns(cfg.MaxOpenConns)
	}
	if cfg.MaxIdleConns != 0 {
		db.SetMaxIdleConns(cfg.MaxIdleConns)
	}

	return db, nil
}
