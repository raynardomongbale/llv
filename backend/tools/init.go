package tools

import (
	"errors"
	"fmt"
	"log/slog"
	"sync"

	"github.com/caarlos0/env/v6"
	"github.com/jmoiron/sqlx"
	"github.com/raynard2/my_errors"
)

var (
	mtx    sync.Mutex
	dbRef  int
	logRef int
	// DB is the global database pool
	DB *sqlx.DB
	// Log is the global logger
	Log *slog.Logger
)

// GlobalConfig represents  configuration
type GlobalConfig struct {
	SQL *DBConfig
}

func Init(cfg GlobalConfig, l *slog.Logger) error {

	// Logging must be initialised first.
	if err := InitLogger(l); err != nil && !errors.Is(err, my_errors.ErrNilLogger) {
		return err
	}

	if err := InitDatabase(cfg.SQL); err != nil && !errors.Is(err, my_errors.ErrNilDatabaseConfig) {
		return err
	}

	return nil
}

// InitDatabase initialises the database pool
func InitDatabase(cfg *DBConfig) (err error) {
	mtx.Lock()
	defer mtx.Unlock()

	if nil == cfg {
		return my_errors.ErrNilDatabaseConfig
	}

	dbRef++
	if DB != nil {
		// Already opened
		return errors.New("database already opened")
	}

	DB, err = Open(*cfg)
	if err != nil {
		return err
	}

	return nil
}

// InitGlobalDatabaseFromEnv initialises the global database pool from env config
func InitGlobalDatabaseFromEnv() (err error) {
	var cfg DBConfig
	if err := env.Parse(&cfg); err != nil {
		return my_errors.ErrNilDatabaseConfig
	}

	return InitDatabase(&cfg)
}

// InitLogger initialises the logger
func InitLogger(l *slog.Logger) error {
	mtx.Lock()
	defer mtx.Unlock()

	if nil == l {
		return my_errors.ErrNilLogger
	}

	logRef++
	if Log != nil {
		// Already opened
		return nil
	}

	Log = l
	fmt.Print("init logger", Log)
	return nil
}
