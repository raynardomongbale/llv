package booking

import (
	"github.com/labstack/echo/v4"
	"github.com/raynard2/controllers"
	"github.com/raynard2/models"
	"github.com/raynard2/tools"
)

func PendingApprovalList(c echo.Context) error {
	var pendingState int = 1
	bookings, err := models.BookingsByStateid(tools.DB, int64(pendingState))
	if err != nil {
		return controllers.InternalError(c, err.Error())
	}

	var bookingsResp []controllers.BookingResponse
	for _, booking := range bookings {
		user, err := models.UsersByUserid(tools.DB, booking.Userid)
		if err != nil {
			return controllers.InternalError(c, err.Error())
		}
		course, err := models.CoursesByCourseid(tools.DB, booking.Courseid)
		if err != nil {
			if err.Error() == "sql: no rows in result set" {
				return controllers.BadRequestResponse(c, "Course not found")
			}
			return controllers.InternalError(c, err.Error())
		}
		org, err := models.OrganizationByOrganizationid(tools.DB, course.Orgid)
		if err != nil {
			tools.Log.Error("could not find organization", "err", err)
		}

		bank, err := models.BankAccountById(tools.DB, org.BankAccountId.Int64)
		if err != nil {
			tools.Log.Error("could not find bank details", "err", err)
		}
		bookingResp := controllers.BookingResponse{
			BookingId:         int(booking.Bookingid),
			CourseResponse:    controllers.BuildCourseResponse(course, booking, org, bank),
			UserLoginResponse: controllers.BuildUserResponse(user),
		}
		bookingsResp = append(bookingsResp, bookingResp)
	}

	return controllers.DataResponse(c, 200, bookingsResp)
}
