package booking

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	"github.com/raynard2/auth"
	"github.com/raynard2/controllers"
	"github.com/raynard2/models"
	"github.com/raynard2/tools"
)

func ApproveEnrollment(c echo.Context) error {
	enrollParams := new(struct{ BookingId int })
	err := c.Bind(enrollParams)
	if err != nil {
		return controllers.InternalError(c, err.Error())
	}

	booking, err := models.BookingsByBookingid(tools.DB, int64(enrollParams.BookingId))
	if err != nil {
		return controllers.InternalError(c, err.Error())
	}

	course, err := models.CoursesByCourseid(tools.DB, booking.Courseid)
	if err != nil {
		if err.Error() == "sql: no rows in result set" {
			return controllers.BadRequestResponse(c, "Course not found")
		}
		return controllers.InternalError(c, err.Error())
	}
	var enrolledState int = 2
	// update booking state
	booking.Stateid = int64(controllers.EnrollmentState(enrolledState))
	if err := booking.Update(tools.DB); err != nil {
		return controllers.InternalError(c, err.Error())
	}

	course.Enrolled++
	if err := course.Update(tools.DB); err != nil {
		return controllers.InternalError(c, err.Error())
	}

	claims, err := auth.GetTokenRequest(c)
	if err != nil {
		return controllers.UnauthorizedResponse(c, err.Error())
	}

	userId := claims.UserID
	id, err := strconv.Atoi(userId)
	if err != nil {
		return controllers.BadRequestResponse(c, "Could not convert userId to int")
	}
	user, err := models.UsersByUserid(tools.DB, int64(id))
	if err != nil {
		return controllers.InternalError(c, err.Error())
	}
	org, err := models.OrganizationByOrganizationid(tools.DB, course.Orgid)
	if err != nil {
		tools.Log.Error("could not find organization", "err", err)
	}

	bank, err := models.BankAccountById(tools.DB, org.BankAccountId.Int64)
	if err != nil {
		tools.Log.Error("could not find bank details", "err", err)
	}

	bookingResp := controllers.BookingResponse{
		BookingId:         int(booking.Bookingid),
		CourseResponse:    controllers.BuildCourseResponse(course, booking, org, bank),
		UserLoginResponse: controllers.BuildUserResponse(user),
	}

	return controllers.DataResponse(c, http.StatusOK, bookingResp)
}
