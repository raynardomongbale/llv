package organization

import (
	"database/sql"

	"github.com/labstack/echo/v4"
	"github.com/raynard2/controllers"
	"github.com/raynard2/models"
	"github.com/raynard2/tools"
)

func GetOrganization(c echo.Context) error {

	orgID := c.Param("name")

	org, err := models.OrganizationByName(tools.DB, sql.NullString{
		String: orgID,
		Valid:  true,
	})
	if err != nil {
		return controllers.InternalError(c, err.Error())
	}

	bank, err := models.BankAccountById(tools.DB, org[0].BankAccountId.Int64)
	if err != nil {
		tools.Log.Error("could not find organization", "err", err)
	}

	return controllers.DataResponse(c, 200, controllers.BuildOrganizationResponse(org[0], bank))
}
