package organization

import (
	"database/sql"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/raynard2/controllers"
	"github.com/raynard2/models"
	"github.com/raynard2/tools"
)

func CreateOrganization(c echo.Context) error {
	l := tools.Log
	org := new(controllers.OrganizationParams)
	if err := c.Bind(org); err != nil {
		return controllers.BadRequestResponse(c, "Could not bind organization")
	}

	bankAccount, err := models.BankAccountByAccountNumber(tools.DB, sql.NullString{String: org.AccountNumber, Valid: true})
	if err != nil {
		l.Error("could not find bank account", "err", err)
		return c.Redirect(http.StatusNotFound, "/admin/bankAccount/create")
	}
	if len(bankAccount) == 0 {
		return controllers.BadRequestResponse(c, "Bank account not found")
	}

	newOrg := models.Organization{
		Name:          sql.NullString{String: org.Name, Valid: true},
		Town:          sql.NullString{String: org.Town, Valid: true},
		Postcode:      sql.NullString{String: org.Postcode, Valid: true},
		Country:       sql.NullString{String: org.Country, Valid: true},
		Telephone:     sql.NullString{String: org.Telephone, Valid: true},
		Email:         org.Email,
		Website:       sql.NullString{String: org.Website, Valid: true},
		BankAccountId: sql.NullInt64{Int64: bankAccount[0].Id, Valid: true},
	}
	if err := newOrg.Insert(tools.DB); err != nil {
		return controllers.InternalError(c, err.Error())
	}

	return controllers.DataResponse(c, http.StatusCreated, org)
}
