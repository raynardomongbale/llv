package organization

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/raynard2/controllers"
	"github.com/raynard2/models"
	"github.com/raynard2/tools"
)

func ListOrganization(c echo.Context) error {
	orgs, err := AllOrgs(tools.DB)
	l := tools.Log

	if err != nil {
		return controllers.InternalError(c, err.Error())
	}

	orgResp := make([]controllers.OrganizationParams, len(orgs))
	for i, org := range orgs {
		l.Info("Organization", "org", org)
		bank, err := models.BankAccountById(tools.DB, org.BankAccountId.Int64)
		if err != nil {
			tools.Log.Error("could not find organization", "err", err)
			return controllers.BadRequestResponse(c, "Could not find bank details")
		}
		orgResp[i] = controllers.BuildOrganizationResponse(org, bank)
	}
	l.Info("Organization", "org", orgResp)
	return controllers.DataResponse(c, http.StatusOK, orgResp)

}

func AllOrgs(db models.DB) ([]*models.Organization, error) {

	const sqlstr = "SELECT `organizationid`,`name`,`town`,`county`,`postcode`,`country`,`telephone`,`email`,`website`,`bank_account_id` FROM organization"
	ret := make([]*models.Organization, 0)
	rows, err := db.Query(sqlstr)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var m models.Organization
		if err := rows.Scan(&m.Organizationid, &m.Name, &m.Town, &m.County, &m.Postcode, &m.Country, &m.Telephone, &m.Email, &m.Website, &m.BankAccountId); err != nil {
			return nil, err
		}
		ret = append(ret, &m)
	}

	return ret, rows.Err()
}
