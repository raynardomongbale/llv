package bankAccount

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/raynard2/controllers"
	"github.com/raynard2/models"
	"github.com/raynard2/tools"
)

func ListBankAccount(c echo.Context) error {
	orgs, err := AllBankAccounts(tools.DB)

	if err != nil {
		return controllers.InternalError(c, err.Error())
	}

	bankResp := make([]controllers.BankAccountParams, len(orgs))
	for i, org := range orgs {

		bank, err := models.BankAccountById(tools.DB, org.Id)
		if err != nil {
			tools.Log.Error("could not find organization", "err", err)
		}
		bankResp[i] = controllers.BankAccountParams{
			AccountNumber: bank.AccountNumber.String,
			BankName:      bank.BankName.String,
			AccountName:   bank.AccountHolderName.String,
		}
	}
	return controllers.DataResponse(c, http.StatusOK, bankResp)
}

func AllBankAccounts(db models.DB) ([]*models.BankAccount, error) {

	const sqlstr = "SELECT `id`,`account_number`,`bank_name`,`account_holder_name` FROM bank_account"
	ret := make([]*models.BankAccount, 0)
	rows, err := db.Query(sqlstr)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var m models.BankAccount
		if err := rows.Scan(&m.Id, &m.AccountNumber, &m.BankName, &m.AccountHolderName); err != nil {
			return nil, err
		}
		ret = append(ret, &m)
	}

	return ret, rows.Err()
}
