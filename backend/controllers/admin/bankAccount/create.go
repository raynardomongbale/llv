package bankAccount

import (
	"database/sql"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/raynard2/controllers"
	"github.com/raynard2/models"
	"github.com/raynard2/tools"
)

func CreateBankAccount(c echo.Context) error {
	params := new(controllers.BankAccountParams)

	if err := c.Bind(params); err != nil {
		return controllers.BadRequestResponse(c, err.Error())
	}

	if params.AccountNumber == "" && params.BankName == "" && params.AccountName == "" {
		return controllers.BadRequestResponse(c, "invalid body")
	}

	bankAccount := models.BankAccount{
		AccountNumber: sql.NullString{
			String: params.AccountNumber,
			Valid:  true,
		},
		BankName: sql.NullString{
			String: params.BankName,
			Valid:  true,
		},
		AccountHolderName: sql.NullString{
			String: params.AccountName,
			Valid:  true,
		},
	}

	err := bankAccount.Insert(tools.DB)
	if err != nil {
		return controllers.InternalError(c, err.Error())
	}

	return controllers.DataResponse(c, http.StatusOK, bankAccount)
}
