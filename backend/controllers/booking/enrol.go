package booking

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	"github.com/raynard2/auth"
	"github.com/raynard2/controllers"
	"github.com/raynard2/models"
	"github.com/raynard2/tools"
)

func Enroll(c echo.Context) error {
	l := tools.Log
	enrollParams := new(controllers.CourseEnrollParams)
	err := c.Bind(enrollParams)
	if err != nil {
		return controllers.InternalError(c, err.Error())
	}
	course, err := models.CoursesByCourseid(tools.DB, int64(enrollParams.CourseId))
	if err != nil {
		if err.Error() == "sql: no rows in result set" {
			return controllers.BadRequestResponse(c, "Course not found")
		}
		return controllers.InternalError(c, err.Error())
	}

	claims, err := auth.GetTokenRequest(c)
	if err != nil {
		return controllers.UnauthorizedResponse(c, err.Error())
	}

	userId := claims.UserID
	id, err := strconv.Atoi(userId)
	if err != nil {
		return controllers.BadRequestResponse(c, "Could not convert userId to int")
	}

	boookings, err := models.BookingsByUseridCourseid(tools.DB, int64(id), course.Courseid)
	if err != nil {
		return controllers.InternalError(c, err.Error())
	}
	if len(boookings) > 0 {
		return controllers.BadRequestResponse(c, "You are already enrolled in this course")
	}

	booking := models.Bookings{
		Userid:   int64(id),
		Courseid: course.Courseid,
		Stateid:  int64(controllers.EnrollmentState(1)),
	}

	if err = booking.Insert(tools.DB); err != nil {
		l.Error("error", "err", err)
		return controllers.InternalError(c, err.Error())
	}
	return controllers.DataResponse(c, http.StatusOK, course)
}
