package booking

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	"github.com/raynard2/controllers"
	"github.com/raynard2/models"
	"github.com/raynard2/tools"
)

func Get(c echo.Context) error {
	bookingId, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return controllers.BadRequestResponse(c, err.Error())
	}
	booking, err := models.BookingsByBookingid(tools.DB, int64(bookingId))
	if err != nil {
		return controllers.InternalError(c, err.Error())
	}

	course, err := models.CoursesByCourseid(tools.DB, booking.Courseid)
	if err != nil {
		return controllers.BadRequestResponse(c, "Could not find course")
	}
	org, err := models.OrganizationByOrganizationid(tools.DB, course.Orgid)
	if err != nil {
		tools.Log.Error("could not find organization", "err", err)
	}

	bank, err := models.BankAccountById(tools.DB, org.BankAccountId.Int64)
	if err != nil {
		tools.Log.Error("could not find bank details", "err", err)
	}
	return controllers.DataResponse(c, http.StatusOK, controllers.BuildCourseResponse(course, booking, org, bank))
}
