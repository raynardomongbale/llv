package users

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func Logout(c echo.Context) error {

	return c.Redirect(http.StatusMovedPermanently, c.Echo().Reverse("/login"))
}
