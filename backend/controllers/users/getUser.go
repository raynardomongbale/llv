package users

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/raynard2/auth"
	"github.com/raynard2/controllers"
	"github.com/raynard2/models"
	"github.com/raynard2/my_errors"
	"github.com/raynard2/tools"
)

func GetUser(c echo.Context) error {
	var users []*models.Users
	var err error

	claim, err := auth.GetTokenRequest(c)
	if err != nil {
		return controllers.UnauthorizedResponse(c, err.Error())
	}

	users, err = models.UsersByEmail(tools.DB, claim.Name)

	if err != nil {
		tools.Log.Error("could not retrieve users info", "err", err)
		return controllers.InternalError(c, err.Error())
	}

	if len(users) == 0 {
		return controllers.NotAuthorizedResponse(c, my_errors.ErrUserNotFound.Error())
	}

	user := users[0]
	ret := controllers.UserLoginResponse{
		Id:         int(user.Userid),
		Title:      user.Title,
		Username:   user.Username,
		Firstname:  user.Firstname,
		Surname:    user.Surname,
		Email:      user.Email,
		Town:       user.Town.String,
		Country:    user.Country.String,
		Sex:        user.Issexmale,
		Telephone:  user.Telephone.String,
		Postalcode: user.Postcode.String,
	}

	return controllers.DataResponse(c, http.StatusAccepted, ret)
}
