package users

import (
	"strconv"

	"github.com/labstack/echo/v4"
	"github.com/raynard2/auth"
	"github.com/raynard2/controllers"
	"github.com/raynard2/models"
	"github.com/raynard2/tools"
)

func Enrollment(c echo.Context) error {

	claims, err := auth.GetTokenRequest(c)
	if err != nil {
		return controllers.UnauthorizedResponse(c, err.Error())
	}

	userId := claims.UserID
	id, err := strconv.Atoi(userId)
	if err != nil {
		return controllers.BadRequestResponse(c, "Could not convert userId to int")
	}
	bookings, err := models.BookingsByUserid(tools.DB, int64(id))
	if err != nil {
		return controllers.BadRequestResponse(c, "Could not find bookings")
	}

	var courseResponse []controllers.CourseResponse

	for _, booking := range bookings {
		course, err := models.CoursesByCourseid(tools.DB, booking.Courseid)
		if err != nil {
			return controllers.BadRequestResponse(c, "Could not find course")
		}
		org, err := models.OrganizationByOrganizationid(tools.DB, course.Orgid)
		if err != nil {
			tools.Log.Error("could not find organization", "err", err)
		}

		bank, err := models.BankAccountById(tools.DB, org.BankAccountId.Int64)
		if err != nil {
			tools.Log.Error("could not find bank details", "err", err)
		}
		courseResponse = append(courseResponse, controllers.BuildCourseResponse(course, booking, org, bank))
	}
	return controllers.InternalError(c, "could not find booking")

}
