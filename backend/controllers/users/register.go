package users

import (
	"database/sql"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	"github.com/raynard2/auth"
	"github.com/raynard2/controllers"
	"github.com/raynard2/models"
	"github.com/raynard2/my_errors"
	"github.com/raynard2/tools"
)

var err error

func Register(c echo.Context) error {
	params := new(controllers.UserParams)

	if err := c.Bind(params); err != nil {

		return controllers.BadRequestResponse(c, my_errors.INVALID_BODY.Error())
	}

	if params.Email == "" || params.Password == "" || params.Username == "" {
		return controllers.BadRequestResponse(c, my_errors.INVALID_BODY.Error())
	}
	tools.Log.Info("params", "params", params)
	hashedPassword, err := auth.CreateHashedPassword(params.Password)
	if err != nil {
		tools.Log.Error("error hashing password", "err", err)
		return controllers.InternalError(c, err.Error())
	}

	//get organization id
	var organizationid int64
	org, err := models.OrganizationByName(tools.DB, sql.NullString{
		String: params.Organization,
		Valid:  true,
	})
	if err != nil {
		tools.Log.Error("error getting organization", "err", err)
		return controllers.InternalError(c, err.Error())
	}
	if len(org) == 0 {
		//organization := models.Organization{
		//	Name:     sql.NullString{String: params.Organization, Valid: true},
		//	Town:     sql.NullString{String: params.Town, Valid: true},
		//	Postcode: sql.NullString{String: params.Postalcode, Valid: true},
		//	Country:  sql.NullString{String: params.Country, Valid: true},
		//}
		//
		//if err = organization.Insert(tools.DB); err != nil {
		//	tools.Log.Error("could not insert organization info", "err", err)
		//	return controllers.InternalError(c, err.Error())
		//}
		//organizationid = organization.Organizationid
	} else {
		organizationid = org[0].Organizationid
	}

	sex := controllers.Others
	if params.Sex == "F" {
		sex = controllers.Female
	} else if params.Sex == "M" {
		sex = controllers.Male
	}
	//create user
	user := models.Users{
		Firstname:      params.Firstname,
		Surname:        params.Surname,
		Email:          params.Email,
		Username:       params.Username,
		Cryptpass:      string(hashedPassword),
		Organizationid: sql.NullInt64{Int64: organizationid},
		Telephone:      sql.NullString{String: params.Telephone, Valid: true},
		Town:           sql.NullString{String: params.Town, Valid: true},
		Postcode:       sql.NullString{String: params.Postalcode, Valid: true},
		Country:        sql.NullString{String: params.Country, Valid: true},
		Title:          params.Title,
		Issexmale:      string(rune(sex)),
		Ishigheradmin:  sql.NullString{String: "0", Valid: true},
	}

	if err = user.Insert(tools.DB); err != nil {
		tools.Log.Error("could not insert user info", "err", err)
		return controllers.InternalError(c, err.Error())
	}

	ret := controllers.BuildUserResponse(&user)

	var token string
	if token, err = auth.GenerateTokensAndSetCookies(strconv.FormatInt(user.Userid, 10), user.Email, ret.Isadmin, c); err != nil {
		return echo.NewHTTPError(http.StatusUnauthorized, "Token is incorrect")
	}

	return controllers.AuthedResponse(c, http.StatusCreated, ret, token)
}
