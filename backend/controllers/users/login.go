package users

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	"github.com/raynard2/auth"
	"github.com/raynard2/controllers"
	"github.com/raynard2/models"
	"github.com/raynard2/my_errors"
	"github.com/raynard2/tools"
)

func Login(c echo.Context) error {
	var users []*models.Users
	var err error
	l := tools.Log
	if c.FormValue("email") != "" {
		users, err = models.UsersByEmail(tools.DB, c.FormValue("email"))
		if err != nil {
			l.Error("could not retrieve users info", "err", err)
			return err
		}
		//users, err = models.UsersByEmail(tools.DB, c.FormValue("email"))
	} else if c.FormValue("username") != "" {
		users, err = models.UsersByUsername(tools.DB, c.FormValue("username"))
	}

	if err != nil {
		l.Error("could not retrieve users info", "err", err)
		return controllers.InternalError(c, err.Error())
	}
	if len(users) == 0 {
		return controllers.NotAuthorizedResponse(c, my_errors.ErrUserNotFound.Error())
	}
	if !auth.VerifyUserCred(c.FormValue("password"), users[0].Cryptpass) {
		l.Error("password did not match", "err", my_errors.ErrUnMatchPassword)
		return controllers.NotAuthorizedResponse(c, my_errors.ErrUnMatchPassword.Error())
	}

	user := users[0]
	ret := controllers.BuildUserResponse(user)
	var token string
	if token, err = auth.GenerateTokensAndSetCookies(strconv.FormatInt(users[0].Userid, 10), user.Email, ret.Isadmin, c); err != nil {
		l.Error("could not generate token", "err", err)
		return echo.NewHTTPError(http.StatusUnauthorized, "Token is incorrect")
	}

	return controllers.AuthedResponse(c, http.StatusAccepted, ret, token)
}
