package controllers

import (
	"github.com/raynard2/models"
)

type UserParams struct {
	Title        string `json:"title"`
	Username     string `json:"username"`
	Firstname    string `json:"firstname"`
	Surname      string `json:"surname"`
	Email        string `json:"email"`
	Organization string `json:"organization"`
	Town         string `json:"town"`
	Country      string `json:"country"`
	Sex          string `json:"sex"`
	Telephone    string `json:"telephone"`
	Postalcode   string `json:"postalcode"`
	Password     string `json:"password"`
	Isadmin      string `json:"isadmin"`
}

type UserLoginResponse struct {
	Id           int    `json:"id"`
	Title        string `json:"title"`
	Username     string `json:"username"`
	Firstname    string `json:"firstname"`
	Surname      string `json:"surname"`
	Email        string `json:"email"`
	Organization string `json:"organization"`
	Town         string `json:"town"`
	Country      string `json:"country"`
	Sex          string `json:"sex"`
	Telephone    string `json:"telephone"`
	Postalcode   string `json:"postalcode"`
	Password     string `json:"password"`
	Isadmin      bool   `json:"isadmin"`
}

type CreateCourseParams struct {
	Name                  string `json:"name"`
	Startdate             string `json:"startdate"`
	Enddate               string `json:"enddate"`
	FinalRegistrationDate string `json:"final_registration_date"`
	Description           string `json:"description"`
	Size                  string `json:"size"`
	Town                  string `json:"town"`
	Country               string `json:"country"`
	Price                 string `json:"price"`
	Currency              string `json:"currency"`
	OrganizationId        int    `json:"organizationId"`
}

type BankAccountParams struct {
	AccountId     int    `json:"account_id"`
	AccountNumber string `json:"account_number"`
	BankName      string `json:"bank_name"`
	AccountName   string `json:"account_name"`
}

type OrganizationParams struct {
	Id        string `json:"id"`
	Name      string `json:"name"`
	Town      string `json:"town"`
	Postcode  string `json:"postcode"`
	Country   string `json:"country"`
	Telephone string `json:"telephone"`
	Email     string `json:"email"`
	Website   string `json:"website"`
	BankAccountParams
}

type CourseResponse struct {
	Id                    int64  `json:"id"`
	Name                  string `json:"name"`
	Size                  int    `json:"size"`
	Enrolled              int    `json:"enrolled"`
	Description           string `json:"description"`
	Startdate             string `json:"startdate"`
	Enddate               string `json:"enddate"`
	FinalRegistrationDate string `json:"final_registration_date"`
	Town                  string `json:"town"`
	Country               string `json:"country"`
	State                 string `json:"state"`
	Price                 string `json:"price"`
	Currency              string `json:"currency"`
	OrganizationParams
}

type CourseEnrollParams struct {
	CourseId int `json:"courseId"`
}

type ProfileResponse struct {
	User     *models.Users
	Bookings []*models.Courses
}

type EnrollmentState int

type BookingResponse struct {
	BookingId int `json:"bookingId"`
	CourseResponse
	UserLoginResponse
}

type Sex int

const (
	Male Sex = iota
	Female
	Others
)

const (
	None EnrollmentState = iota
	Pending
	Enrolled
	Cancelled
	Unknown
)

type Organization struct {
	Id          int    `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Email       string `json:"email"`
	Telephone   string `json:"phone"`
	Website     string `json:"website"`
	Town        string `json:"town"`
	Postcode    string `json:"postcode"`
	Country     string `json:"country"`
}
