package controllers

import (
	"strconv"
	"time"

	"github.com/raynard2/models"
)

func BuildCourseResponse(course *models.Courses, booking *models.Bookings, org *models.Organization, bank *models.BankAccount) CourseResponse {

	var state = CourseState(0)
	if booking != nil {
		state = CourseState(int(booking.Stateid))
	}

	return CourseResponse{
		Id:                    course.Courseid,
		Name:                  course.Name.String,
		Size:                  course.Size,
		Description:           course.Description.String,
		Town:                  course.Town.String,
		Country:               course.Country.String,
		Startdate:             course.Startdate.Format(time.RFC850),
		Enddate:               course.Enddate.Format(time.RFC850),
		FinalRegistrationDate: course.Finalregistrationdate.Format(time.RFC850),
		State:                 state,
		Price:                 course.Price.String,
		Currency:              course.Currency.String,
		OrganizationParams: OrganizationParams{
			Name:      org.Name.String,
			Town:      org.Town.String,
			Postcode:  org.Postcode.String,
			Country:   org.Country.String,
			Telephone: org.Telephone.String,
			Email:     org.Email,
			Website:   org.Website.String,
			BankAccountParams: BankAccountParams{
				AccountNumber: bank.AccountNumber.String,
				BankName:      bank.BankName.String,
				AccountName:   bank.AccountHolderName.String,
			},
		},
	}
}

func CourseState(state int) string {
	var stateMap map[int]string = map[int]string{
		0: "Not Enrolled",
		1: "Pending",
		2: "Enrolled",
		4: "Unknown",
	}
	return stateMap[state]
}

func BuildUserResponse(user *models.Users) UserLoginResponse {

	isAdmin := false
	if user.Ishigheradmin.String == "1" {
		isAdmin = true
	}

	return UserLoginResponse{
		Id:         int(user.Userid),
		Title:      user.Title,
		Username:   user.Username,
		Firstname:  user.Firstname,
		Surname:    user.Surname,
		Email:      user.Email,
		Town:       user.Town.String,
		Country:    user.Country.String,
		Sex:        user.Issexmale,
		Telephone:  user.Telephone.String,
		Postalcode: user.Postcode.String,
		Isadmin:    isAdmin,
	}
}

func BuildOrganizationResponse(org *models.Organization, bank *models.BankAccount) OrganizationParams {
	return OrganizationParams{
		Id:        strconv.FormatInt(org.Organizationid, 10),
		Name:      org.Name.String,
		Town:      org.Town.String,
		Postcode:  org.Postcode.String,
		Country:   org.Country.String,
		Telephone: org.Telephone.String,
		Email:     org.Email,
		Website:   org.Website.String,
		BankAccountParams: BankAccountParams{
			AccountId:     int(bank.Id),
			AccountNumber: bank.AccountNumber.String,
			BankName:      bank.BankName.String,
			AccountName:   bank.AccountHolderName.String,
		},
	}
}
