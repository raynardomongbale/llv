package course

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	"github.com/raynard2/auth"
	"github.com/raynard2/controllers"
	"github.com/raynard2/models"
	"github.com/raynard2/tools"
)

func Get(c echo.Context) error {

	l := tools.Log
	courseId, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		l.Error("could not covert id", "err", err.Error())
		return controllers.BadRequestResponse(c, err.Error())
	}

	course, err := models.CoursesByCourseid(tools.DB, int64(courseId))

	if err != nil {
		return controllers.InternalError(c, err.Error())
	}

	var booking *models.Bookings

	claim, err := auth.GetTokenRequest(c)
	if err != nil {
		l.Info("could retrieve auth, Maybe ot logged in", "err", err)
	}

	if claim != nil {
		userId, err := strconv.Atoi(claim.UserID)
		if err != nil {
			return controllers.BadRequestResponse(c, "Could not convert cookie to int")
		}

		bookings, err := models.BookingsByUseridCourseid(tools.DB, int64(userId), int64(courseId))

		if err != nil {
			l.Error("error", err)
			return controllers.BadRequestResponse(c, "Could not find bookings")
		}
		if len(bookings) > 0 {
			booking = bookings[0]
		}

	}
	org, err := models.OrganizationByOrganizationid(tools.DB, course.Orgid)
	if err != nil {
		tools.Log.Error("could not find organization", "err", err)
	}

	bank, err := models.BankAccountById(tools.DB, org.BankAccountId.Int64)
	if err != nil {
		tools.Log.Error("could not find organization", "err", err)
	}

	return controllers.DataResponse(c, http.StatusOK, controllers.BuildCourseResponse(course, booking, org, bank))
}
