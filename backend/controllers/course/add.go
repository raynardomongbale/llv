package course

import (
	"database/sql"
	"net/http"
	"strconv"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/raynard2/controllers"
	"github.com/raynard2/models"
	"github.com/raynard2/tools"
)

func Create(c echo.Context) error {
	params := new(controllers.CreateCourseParams)

	if err := c.Bind(params); err != nil {
		return controllers.BadRequestResponse(c, err.Error())
	}
	tools.Log.Info("Create course", "params", params)
	if params.Name == "" {
		return controllers.BadRequestResponse(c, "invalid body")
	}

	size, err := strconv.Atoi(params.Size)
	if err != nil {
		return controllers.BadRequestResponse(c, "invalid size")
	}

	startDate, err := time.Parse(time.RFC3339Nano, params.Startdate)
	if err != nil {
		return controllers.BadRequestResponse(c, "invalid start date")
	}

	endDate, err := time.Parse(time.RFC3339Nano, params.Enddate)
	if err != nil {
		return controllers.BadRequestResponse(c, "invalid end date")
	}

	finalregistrationdate, err := time.Parse(time.RFC3339Nano, params.FinalRegistrationDate)
	if err != nil {
		return controllers.BadRequestResponse(c, "invalid final registration date")
	}

	course := models.Courses{
		Name:                  sql.NullString{String: params.Name, Valid: true},
		Size:                  size,
		Description:           sql.NullString{String: params.Description, Valid: true},
		Town:                  sql.NullString{String: params.Town, Valid: true},
		Country:               sql.NullString{String: params.Country, Valid: true},
		Enrolled:              0,
		Startdate:             startDate,
		Enddate:               endDate,
		Finalregistrationdate: finalregistrationdate,
		Price:                 sql.NullString{String: params.Price, Valid: true},
		Currency:              sql.NullString{String: params.Currency, Valid: true},
		Orgid:                 int64(params.OrganizationId),
	}

	if err := course.Insert(tools.DB); err != nil {
		return controllers.InternalError(c, err.Error())
	}

	return controllers.DataResponse(c, http.StatusCreated, course)
}
