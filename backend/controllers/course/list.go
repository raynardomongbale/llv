package course

import (
	"database/sql"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	"github.com/raynard2/auth"
	"github.com/raynard2/controllers"
	"github.com/raynard2/models"
	"github.com/raynard2/tools"
)

func List(c echo.Context) error {
	l := tools.Log
	courses, err := AllCourses(tools.DB)

	if err != nil {
		return controllers.InternalError(c, err.Error())
	}
	claim, _ := auth.GetTokenRequest(c)

	courseResponse := make([]controllers.CourseResponse, len(courses))
	for i, course := range courses {
		var currBooking *models.Bookings
		if claim != nil {
			userID, err := strconv.Atoi(claim.UserID)
			if err != nil {
				return controllers.InternalError(c, err.Error())
			}
			booking, err := models.BookingsByUseridCourseid(tools.DB, int64(userID), course.Courseid)
			if err != nil {
				return controllers.InternalError(c, err.Error())
			}
			if len(booking) > 0 {
				currBooking = booking[0]
			}
		}
		l.Info("course", "course", course)
		org, err := models.OrganizationByOrganizationid(tools.DB, course.Orgid)

		if err != nil {
			l.Error("could not find organization", "err", err)
			continue
		}

		bank, err := models.BankAccountById(tools.DB, org.BankAccountId.Int64)

		if err != nil {
			l.Error("could not find organization", "err", err)
			continue
		}
		courseResponse[i] = controllers.BuildCourseResponse(course, currBooking, org, bank)
	}
	return controllers.DataResponse(c, http.StatusOK, courseResponse)
}

func GetByName(c echo.Context) error {

	l := tools.Log
	l.Info("GetByName")
	name := c.QueryParam("name")

	course, err := models.CoursesByName(tools.DB, sql.NullString{
		String: name,
		Valid:  true,
	})
	if err != nil {
		tools.Log.Info(err.Error())
		return controllers.BadRequestResponse(c, err.Error())
	}
	return controllers.DataResponse(c, http.StatusOK, course)
}

func AllCourses(db models.DB) ([]*models.Courses, error) {
	const sqlstr = "SELECT `courseid`,`name`,`size`,`enrolled`,`description`,`startdate`,`enddate`,`finalregistrationdate`,`town`,`country`, `price`, `currency`, `orgid` FROM courses"

	ret := make([]*models.Courses, 0)
	rows, err := db.Query(sqlstr)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var m models.Courses
		if err := rows.Scan(&m.Courseid, &m.Name, &m.Size, &m.Enrolled, &m.Description, &m.Startdate, &m.Enddate, &m.Finalregistrationdate, &m.Town, &m.Country, &m.Price, &m.Currency, &m.Orgid); err != nil {
			return nil, err
		}
		ret = append(ret, &m)
	}

	return ret, rows.Err()
}
