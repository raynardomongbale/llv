package auth

import (
	"echo"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestProcess(t *testing.T) {
	type testCase struct {
		Name string

		Next echo.HandlerFunc

		ExpectedHandlerFunc echo.HandlerFunc
	}

	validate := func(t *testing.T, tc *testCase) {
		t.Run(tc.Name, func(t *testing.T) {
			actualHandlerFunc := Process(tc.Next)

			assert.Equal(t, tc.ExpectedHandlerFunc, actualHandlerFunc)
		})
	}

	validate(t, &testCase{})
}
