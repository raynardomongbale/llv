package auth

import (
	"golang.org/x/crypto/bcrypt"
)

func VerifyUserCred(plainPW, cryptPW string) bool {
	return bcrypt.CompareHashAndPassword([]byte(cryptPW), []byte(plainPW)) == nil
}

func CreateHashedPassword(plainPW string) ([]byte, error) {
	password, err := bcrypt.GenerateFromPassword([]byte(plainPW), bcrypt.DefaultCost)
	if err != nil {
		return []byte{}, nil
	}
	return password, nil
}
