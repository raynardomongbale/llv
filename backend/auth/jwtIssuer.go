package auth

import (
	"errors"
	"net/http"
	"strings"
	"time"

	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
	"github.com/raynard2/tools"
)

const (
	accessTokenCookieName = "_auth"
	jwtSecretKey          = "some-secret-key"
)

func GetJWTSecret() string {
	return jwtSecretKey
}

type Claims struct {
	Name   string `json:"username"`
	UserID string `json:"userID"`
	Admin  bool   `json:"admin"`
	jwt.StandardClaims
}

// GenerateTokensAndSetCookies generates jwt token and saves it to the http-only cookie.
func GenerateTokensAndSetCookies(userId, username string, isAdmin bool, c echo.Context) (string, error) {
	accessToken, exp, err := generateAccessToken(userId, username, isAdmin)
	if err != nil {
		return "nil", err
	}

	setTokenCookie(accessTokenCookieName, accessToken, exp, c)
	setUserCookie(username, exp, c)
	setUserIDCookie(userId, exp, c)
	return accessToken, nil
}

func GetTokenRequest(c echo.Context) (*Claims, error) {
	l := tools.Log
	auth := c.Request().Header.Get("Authorization")
	var err error
	if auth == "" {
		l.Error("No authorization in header")
		return nil, errors.New("authorization Header Not Found")
	}
	splitToken := strings.Split(auth, "Bearer ")
	auth = splitToken[1]

	token, err := jwt.ParseWithClaims(auth, &Claims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(GetJWTSecret()), nil
	})

	if err != nil {
		return nil, errors.New("authorization Header Not Found")
	}
	if claims, ok := token.Claims.(*Claims); ok && token.Valid {
		return claims, nil
	}
	l.Error("Authorization Header Not Found")
	return nil, errors.New("authorization Header Not Found")
}

func generateAccessToken(userID, username string, isAdmin bool) (string, time.Time, error) {
	// Declare the expiration time of the token (1h).
	expirationTime := time.Now().Add(1 * time.Hour)

	return generateToken(userID, username, isAdmin, expirationTime, []byte(GetJWTSecret()))
}

func generateToken(userID, username string, isAdmin bool, expirationTime time.Time, secret []byte) (string, time.Time, error) {
	claims := &Claims{
		Name:   username,
		UserID: userID,
		Admin:  isAdmin,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	tokenString, err := token.SignedString(secret)
	if err != nil {
		return "", time.Now(), err
	}

	return tokenString, expirationTime, nil
}

// Here we are creating a new cookie, which will store the valid JWT token.
func setTokenCookie(name, token string, expiration time.Time, c echo.Context) {
	cookie := new(http.Cookie)
	cookie.Name = name
	cookie.Value = token
	cookie.Expires = expiration
	cookie.Path = "/"
	// Http-only helps mitigate the risk of client side script accessing the protected cookie.
	cookie.HttpOnly = true

	c.SetCookie(cookie)
}

// Purpose of this cookie is to store the user's name.
func setUserCookie(user string, expiration time.Time, c echo.Context) {
	cookie := new(http.Cookie)
	cookie.Name = "user"
	cookie.Value = user
	cookie.Expires = expiration
	cookie.Path = "/"
	c.SetCookie(cookie)
}

// Purpose of this cookie is to store the user's id.
func setUserIDCookie(userID string, expiration time.Time, c echo.Context) {
	cookie := new(http.Cookie)
	cookie.Name = "user-id"
	cookie.Value = userID
	cookie.Expires = expiration
	cookie.Path = "/"
	c.SetCookie(cookie)
}

// JWTErrorChecker will be executed when user try to access a protected path.
func JWTErrorChecker(err error, c echo.Context) error {
	// Redirects to the signIn form.

	return c.Redirect(http.StatusMovedPermanently, c.Echo().Reverse("userSignInForm"))
}
