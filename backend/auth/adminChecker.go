package auth

import (
	"github.com/labstack/echo/v4"
	"github.com/raynard2/controllers"
	"github.com/raynard2/tools"
)

func IsAdmin(next echo.HandlerFunc) echo.HandlerFunc {
	l := tools.Log
	return func(c echo.Context) error {
		claim, err := GetTokenRequest(c)
		if err != nil {
			l.Error("Error getting token")
			return err
		}
		if !claim.Admin {
			l.Error("Error: Not an admin")
			return controllers.UnauthorizedResponse(c, "User does not have required privilege to access")
		}
		return next(c)
	}
}
