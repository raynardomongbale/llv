#!/bin/bash

set +e
set +o pipefail

grep -l '// GENERATED' ../models/*.go | xargs -r rm
# shellcheck disable=SC2002
cat xo/generate.txt | xargs -I {} -n1 -r echo xo/{} | xargs schema2go -extension xo.go -output "../models/" -template "../models/templates/*.tpl" -replacements ../models/templates/replacements.txt
grep -l '// GENERATED' ../models/*.go | xargs -r goimports -w
grep -l '// GENERATED' ../models/*.go | xargs -r gofmt -s -w
