CREATE TABLE `users` (
                               `userId` bigint(20) NOT NULL AUTO_INCREMENT,
                               `title` varchar(4) NOT NULL DEFAULT 'Mr',
                               `firstName` varchar(20) NOT NULL DEFAULT '',
                               `surName` varchar(20) NOT NULL DEFAULT '',
                               `organizationId` bigint(20) DEFAULT NULL,
                               `town` varchar(255) DEFAULT NULL,
                               `county` varchar(255) DEFAULT NULL,
                               `postcode` varchar(255) DEFAULT NULL,
                               `country` varchar(255) DEFAULT NULL,
                               `telephone` varchar(255) DEFAULT NULL,
                               `email` varchar(250) NOT NULL DEFAULT '',
                                `username` varchar(255) NOT NULL DEFAULT '',
                                `cryptpass` varchar(255) NOT NULL DEFAULT '',
                               `isHigherAdmin` varchar(30) DEFAULT 'false',
                               `isSexMale` varchar(50) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '0',
                               PRIMARY KEY (`userId`),
                               KEY `idx_organizationId` (`organizationid`),
                               KEY `idx_username` (`username`),
                                 KEY `idx_email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

