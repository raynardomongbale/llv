CREATE TABLE `bank_account`
(
    `id`                  bigint(20) NOT NULL AUTO_INCREMENT,
    `account_number`      varchar(255) DEFAULT NULL,
    `bank_name`           varchar(255) DEFAULT NULL,
    `account_holder_name` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `idx_account_number` (`account_number`),
    KEY `idx_bank_name` (`bank_name`),
    KEY `idx_account_holder_name` (`account_holder_name`)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1;
