package svc

import (
	"log/slog"

	"github.com/kardianos/service"
	"github.com/raynard2/router"
)

var logger service.Logger

type Program struct {
	Port   string
	Logger *slog.Logger
}

func (p *Program) Start(s service.Service) error {
	// Start should not block. Do the actual work async.
	go p.run()
	return nil
}

func (p *Program) run() {

	app := router.NewApp()
	p.Logger.Info("Starting Router")

	if err := app.Start(p.Port); err != nil {
		p.Logger.Error("error starting Logger", err)
	}
}

func (p *Program) Stop(s service.Service) error {
	// Stop should not block. Return with a few seconds.
	return nil
}
