package main

import (
	"github.com/kardianos/service"
	"github.com/raynard2/svc"
	"github.com/raynard2/tools"
	"github.com/raynard2/utils"
)

func main() {
	l := utils.CommonLogger("llv")
	//init components
	if err := tools.Init(
		tools.GlobalConfig{
			SQL: &tools.DBConfig{
				Driver:          "mysql",
				DSN:             "user:password@tcp(llvDB:3306)/dev_apps?parseTime=true",
				ConnMaxIdleTime: 20,
				ConnMaxLifetime: 120,
				MaxIdleConns:    20,
				MaxOpenConns:    100,
			},
		},
		l,
	); nil != err {
		l.Error("error initializing database", "err", err)
		return
	}
	//mysql://user:password@localhost:3306/dev_app?useSSL=false
	svcConfig := &service.Config{
		Name:        "GoServiceExampleSimple",
		DisplayName: "Go Service Example",
		Description: "This is an example Go service.",
	}

	s, err := service.New(
		&svc.Program{
			Port:   ":4000",
			Logger: l,
		},
		svcConfig)
	if err != nil {
		l.Error("err setting up service", err)
		return
	}

	err = s.Run()
	if err != nil {
		l.Error("error starting service", "err", err)
		return
	}

}
