package my_errors

import (
	"errors"
)

// Errors
var (
	ErrNilDatabaseConfig = errors.New("nil database configuration")
	ErrNilLogger         = errors.New("nil log configuration")
	ErrUnMatchPassword   = errors.New("password do not match")
	INVALID_BODY         = errors.New("invalid request body")
	ErrUserNotFound      = errors.New("user not found")
)
