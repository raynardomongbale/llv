package models

import (
	"errors"
	"fmt"
)

// FieldCountMismatchError represents missing fields when serialising a model
// TODO(steve): remove this type we don't need to support perl split decoding
type FieldCountMismatchError struct {
	Type     interface{}
	Found    int
	Expected int
}

// NewFieldCountMismatchError returns a new FieldCountMismatchError for the type t when the expected fields was e and the found fields was f.
func NewFieldCountMismatchError(t interface{}, e, f int) *FieldCountMismatchError {
	return &FieldCountMismatchError{Type: t, Found: f, Expected: e}
}

func (e *FieldCountMismatchError) Error() string {
	return fmt.Sprintf("field count mismatch for %T expected %v fields found %v", e.Type, e.Expected, e.Found)
}

// ErrNoAffectedRows is return if a model update affected no rows
var ErrNoAffectedRows = errors.New("no affected rows")
