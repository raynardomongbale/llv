package models

// GENERATED BY SCHEMA2GO. DO NOT EDIT.

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
)

// Organization represents a row from 'organization'.
type Organization struct {
	Organizationid int64          `db:"organizationid,autoinc,pk"`
	Name           sql.NullString `db:"name"`
	Town           sql.NullString `db:"town"`
	County         sql.NullString `db:"county"`
	Postcode       sql.NullString `db:"postcode"`
	Country        sql.NullString `db:"country"`
	Telephone      sql.NullString `db:"telephone"`
	Email          string         `db:"email"`
	Website        sql.NullString `db:"website"`
	BankAccountId  sql.NullInt64  `db:"bank_account_id"`
}

// Insert inserts the Organization to the database.
func (m *Organization) Insert(db DB) error {
	const sqlstr = "INSERT INTO organization (" +
		"`name`,`town`,`county`,`postcode`,`country`,`telephone`,`email`,`website`,`bank_account_id`" +
		") VALUES (" +
		"?,?,?,?,?,?,?,?,?" +
		")"

	DBLog(sqlstr, m.Name, m.Town, m.County, m.Postcode, m.Country, m.Telephone, m.Email, m.Website, m.BankAccountId)
	res, err := db.Exec(sqlstr, m.Name, m.Town, m.County, m.Postcode, m.Country, m.Telephone, m.Email, m.Website, m.BankAccountId)
	if err != nil {
		return err
	}

	id, err := res.LastInsertId()
	if err != nil {
		return err
	}

	m.Organizationid = int64(id)

	return nil
}

// IsPrimaryKeySet returns true if all primary key fields are set to none zero values
func (m *Organization) IsPrimaryKeySet() bool {
	if !IsKeySet(m.Organizationid) {
		return false
	}

	return true
}

// Update updates the Organization in the database.
func (m *Organization) Update(db DB) error {
	const sqlstr = "UPDATE organization " +
		"SET `name` = ?, `town` = ?, `county` = ?, `postcode` = ?, `country` = ?, `telephone` = ?, `email` = ?, `website` = ?, `bank_account_id` = ? " +
		"WHERE `organizationid` = ?"

	DBLog(sqlstr, m.Name, m.Town, m.County, m.Postcode, m.Country, m.Telephone, m.Email, m.Website, m.BankAccountId, m.Organizationid)
	res, err := db.Exec(sqlstr, m.Name, m.Town, m.County, m.Postcode, m.Country, m.Telephone, m.Email, m.Website, m.BankAccountId, m.Organizationid)
	if err != nil {
		return err
	}

	// Requires clientFoundRows=true
	if i, err := res.RowsAffected(); err != nil {
		return err
	} else if i <= 0 {
		return ErrNoAffectedRows
	}

	return nil
}

// Save saves the Organization to the database.
func (m *Organization) Save(db DB) error {
	if m.IsPrimaryKeySet() {
		return m.Update(db)
	}

	return m.Insert(db)
}

// Delete deletes the Organization from the database.
func (m *Organization) Delete(db DB) (err error) {
	const sqlstr = "DELETE FROM organization WHERE `organizationid` = ?"

	DBLog(sqlstr, m.Organizationid)
	_, err = db.Exec(sqlstr, m.Organizationid)

	return
}

// OrganizationByOrganizationid retrieves a row from 'organization' as a Organization.
//
// Generated from primary key.
func OrganizationByOrganizationid(db DB, organizationid int64) (*Organization, error) {
	const sqlstr = "SELECT `organizationid`,`name`,`town`,`county`,`postcode`,`country`,`telephone`,`email`,`website`,`bank_account_id` " +
		"FROM organization " +
		"WHERE `organizationid` = ?"

	DBLog(sqlstr, organizationid)

	var m Organization
	if err := db.QueryRow(sqlstr, organizationid).Scan(&m.Organizationid, &m.Name, &m.Town, &m.County, &m.Postcode, &m.Country, &m.Telephone, &m.Email, &m.Website, &m.BankAccountId); err != nil {
		return nil, err
	}
	return &m, nil

}

// OrganizationByName retrieves a row from 'organization' as a Organization.
//
// Generated from index 'idx_name' of type 'index'.
func OrganizationByName(db DB, name sql.NullString) ([]*Organization, error) {
	const sqlstr = "SELECT `organizationid`,`name`,`town`,`county`,`postcode`,`country`,`telephone`,`email`,`website`,`bank_account_id` " +
		"FROM organization " +
		"WHERE `name` = ?"

	DBLog(sqlstr, name)

	ret := make([]*Organization, 0)
	rows, err := db.Query(sqlstr, name)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var m Organization
		if err := rows.Scan(&m.Organizationid, &m.Name, &m.Town, &m.County, &m.Postcode, &m.Country, &m.Telephone, &m.Email, &m.Website, &m.BankAccountId); err != nil {
			return nil, err
		}
		ret = append(ret, &m)
	}

	return ret, rows.Err()

}
