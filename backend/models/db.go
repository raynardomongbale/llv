package models

import (
	"database/sql"
	"reflect"

	"github.com/jmoiron/sqlx"
)

// DBTransactioner is the interface that database connections that can utilise
// transactions should implement.
type DBTransactioner interface {
	DB
	Transactioner
}

// DB is the common interface for database operations
//
// This should work with database/sql.DB and database/sql.Tx.
type DB interface {
	Exec(string, ...interface{}) (sql.Result, error)
	Query(string, ...interface{}) (*sql.Rows, error)
	QueryRow(string, ...interface{}) *sql.Row
	// Additional sqlx methods we like
	Get(dest interface{}, query string, args ...interface{}) error
	Select(dest interface{}, query string, args ...interface{}) error
}

// Transactioner is the interface that a database connection that can start
// a transaction should implement.
type Transactioner interface {
	Beginx() (*sqlx.Tx, error)
}

// XODB is a compat alias to DB
type XODB = DB

// DBLog provides the log func used by generated queries.
var DBLog = func(string, ...interface{}) {}

// XOLog is a compat shim for DBLog
var XOLog = func(msg string, args ...interface{}) {
	DBLog(msg, args...)
}

// IsKeySet returns true if
// 1. x is an integer and greater than zero.
// 2. x not an integer and is not the zero value.
// Otherwise returns false
func IsKeySet(x interface{}) bool {
	switch x := x.(type) {
	case int:
		return x > 0
	case int8:
		return x > 0
	case int16:
		return x > 0
	case int32:
		return x > 0
	case int64:
		return x > 0
	case uint:
		return x > 0
	case uint8:
		return x > 0
	case uint16:
		return x > 0
	case uint32:
		return x > 0
	case uint64:
		return x > 0
	}

	return x != reflect.Zero(reflect.TypeOf(x)).Interface()
}
