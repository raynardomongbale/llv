{{- define "type" -}}
    {{- if eq .Type "bigint" -}}
        {{- if .Nullable -}}
            {{- if .Unsigned -}}
                *uint64
            {{- else -}}
                sql.NullInt64
            {{- end -}}
        {{- else -}}
            {{- if .Unsigned -}}
                uint64
            {{- else -}}
                int64
            {{- end -}}
        {{- end -}}
    {{- else if eq .Type "int" -}}
        {{- if .Nullable -}}
            sql.NullInt64
        {{- else -}}
            {{- if .Unsigned -}}
                uint
            {{- else -}}
                int
            {{- end -}}
        {{- end -}}
    {{- else if eq .Type "tinyint" -}}
        {{- if eq .TypeSize 1 -}}
            {{- if .Nullable -}}
                sql.NullBool
            {{- else -}}
                bool
            {{- end -}}
        {{- else -}}
            {{- if .Nullable -}}
                sql.NullInt64
            {{- else -}}
                {{- if .Unsigned -}}
                    uint8
                {{- else -}}
                    int8
                {{- end -}}
            {{- end -}}
        {{- end -}}
    {{- else if eq .Type "smallint" -}}
        {{- if .Nullable -}}
             sql.NullInt64
        {{- else -}}
            {{- if .Unsigned -}}
                uint16
            {{- else -}}
                int16
            {{- end -}}
        {{- end -}}
    {{- else if eq .Type "mediumint" -}}
        {{- if .Nullable -}}
             sql.NullInt64
        {{- else -}}
            {{- if .Unsigned -}}
                uint32
            {{- else -}}
                int32
            {{- end -}}
        {{- end -}}
    {{- else if eq .Type "float" -}}
        {{- if .Nullable -}}
            sql.NullFloat64
        {{- else -}}
            float64
        {{- end -}}
    {{- else if eq .Type "decimal" -}}
        {{- if .Nullable -}}
            sql.NullFloat64
        {{- else -}}
            float64
        {{- end -}}
    {{- else if eq .Type "double" -}}
        {{- if .Nullable -}}
            sql.NullFloat64
        {{- else -}}
            float64
        {{- end -}}
    {{- else if eq .Type "char" -}}
        {{- if .Nullable -}}
            sql.NullString
        {{- else -}}
            string
        {{- end -}}
    {{- else if eq .Type "binary" -}}
        []byte
    {{- else if eq .Type "varchar" -}}
        {{- if .Nullable -}}
            sql.NullString
        {{- else -}}
            string
        {{- end -}}
    {{- else if eq .Type "varbinary" -}}
        []byte
    {{- else if eq .Type "tinyblob" -}}
        []byte
    {{- else if eq .Type "tinytext" -}}
        {{- if .Nullable -}}
            sql.NullString
        {{- else -}}
            string
        {{- end -}}
    {{- else if eq .Type "blob" -}}
        []byte
    {{- else if eq .Type "text" -}}
        {{- if .Nullable -}}
            sql.NullString
        {{- else -}}
            string
        {{- end -}}
    {{- else if eq .Type "mediumblob" -}}
        []byte
    {{- else if eq .Type "mediumtext" -}}
        {{- if .Nullable -}}
            sql.NullString
        {{- else -}}
            string
        {{- end -}}
    {{- else if eq .Type "longblob" -}}
        []byte
    {{- else if eq .Type "longtext" -}}
        {{- if .Nullable -}}
            sql.NullString
        {{- else -}}
            string
        {{- end -}}
    {{- else if eq .Type "enum" -}}
        {{- if .Nullable -}}
            sql.NullString
        {{- else -}}
            string
        {{- end -}}
    {{- else if eq .Type "year" -}}
        {{- if .Nullable -}}
            mysql.NullTime
        {{- else -}}
            time.Time
        {{- end -}}
    {{- else if eq .Type "date" -}}
        {{- if .Nullable -}}
            mysql.NullTime
        {{- else -}}
            time.Time
        {{- end -}}
    {{- else if eq .Type "time" -}}
        {{- if .Nullable -}}
            sql.NullString
        {{- else -}}
            string
        {{- end -}}
    {{- else if eq .Type "datetime" -}}
        {{- if .Nullable -}}
            mysql.NullTime
        {{- else -}}
            time.Time
        {{- end -}}
    {{- else if eq .Type "timestamp" -}}
        {{- if .Nullable -}}
            mysql.NullTime
        {{- else -}}
            time.Time
        {{- end -}}
    {{- end -}}
{{- end -}}
