package cache

import (
	"bytes"
	"encoding/gob"
	"time"

	"github.com/coocood/freecache"
	werr "github.com/pkg/errors"
)

const (
	// DefaultSize is the default maximum number of elements in the cache
	DefaultSize = 1000

	// HashFactor is the hashing factor used when sizing the cache
	HashFactor = 1024

	// KeySize is the size of the key used when sizing the cache
	KeySize = 36

	// DefaultCacheTimeout is the default cache timeout applied to prevent infinitely stored cache
	DefaultCacheTimeout = 60
)

// CacheConfig contains configuration for cache
// nolint: golint
type CacheConfig struct {
	// CacheSize is the maximum number of elements in the cache
	CacheSize int

	// CacheTimeout is the length of time the a key data is kept in the cache
	CacheTimeout time.Duration
}

// Cache provides a high performance cache
type Cache struct {
	*freecache.Cache
	DefaultTimeout int
}

// NewDefault returns a fully initialised Cache with the default settings
func NewDefault() *Cache {
	return New(CacheConfig{})
}

// New returns a fully initialised Cache
func New(cfg CacheConfig) *Cache {
	// size = elements * key size * hash factor
	var size int
	if cfg.CacheSize == 0 {
		size = DefaultSize * KeySize * HashFactor
	} else {
		size = cfg.CacheSize * KeySize * HashFactor
	}

	// prevent from the cache timeout from being infinite
	timeout := int(cfg.CacheTimeout.Seconds())
	if timeout <= 0 {
		timeout = DefaultCacheTimeout
	}

	return &Cache{
		Cache:          freecache.NewCache(size),
		DefaultTimeout: timeout,
	}
}

// GetType returns the cached entry for k decoded into d
func (c *Cache) GetType(k []byte, d interface{}) error {
	v, err := c.Get(k)
	if err != nil {
		return err
	}

	// TODO(steve): use a pool of byte buffers / decoders
	dec := gob.NewDecoder(bytes.NewBuffer(v))
	if err := dec.Decode(d); err != nil {
		return werr.Wrapf(err, "cache: failed to decode %T", d)
	}

	return nil
}

// SetType sets d as the cached entry for k with timeout t
func (c *Cache) SetType(k []byte, d interface{}, t int) error {
	// TODO(steve): use a pool of byte buffers / encoders
	buf := bytes.Buffer{}
	enc := gob.NewEncoder(&buf)
	if err := enc.Encode(d); err != nil {
		return werr.Wrapf(err, "cache: failed to encode %T", d)
	}

	return c.Set(k, buf.Bytes(), t)
}

// IsNotFound returns true if err is a cache entry not found error
func IsNotFound(err error) bool {
	return err == freecache.ErrNotFound
}
