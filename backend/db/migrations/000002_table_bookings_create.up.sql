CREATE TABLE `bookings` (
                            `bookingId` bigint(20) NOT NULL AUTO_INCREMENT,
                            `userId` bigint(20) NOT NULL,
                            `courseId` bigint(20) NOT NULL,
                            `stateId` bigint(20) NOT NULL,
                            PRIMARY KEY (`bookingId`),
                            KEY `idx_userId` (`userId`),
                            Key `idx_userId_courseId` (`userId`, `courseId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;