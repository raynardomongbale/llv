CREATE TABLE `organization`
(
    `organizationId`  bigint(20)   NOT NULL AUTO_INCREMENT,
    `name`            varchar(255)          DEFAULT NULL,
    `town`            varchar(255)          DEFAULT NULL,
    `county`          varchar(255)          DEFAULT NULL,
    `postcode`        varchar(255)          DEFAULT NULL,
    `country`         varchar(255)          DEFAULT NULL,
    `telephone`       varchar(255)          DEFAULT NULL,
    `email`           varchar(250) NOT NULL DEFAULT '',
    `website`         varchar(255)          DEFAULT NULL,
    `bank_account_id` bigint(20)            DEFAULT NULL,
    PRIMARY KEY (`organizationId`),
    KEY `idx_name` (`name`)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1;