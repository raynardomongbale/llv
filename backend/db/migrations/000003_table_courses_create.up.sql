CREATE TABLE `courses`
(
    `courseId`              bigint(20) NOT NULL AUTO_INCREMENT,
    `name`                  varchar(255) DEFAULT NULL,
    `size`                  int(11)    NOT NULL,
    `enrolled`              int(11)    NOT NULL,
    `description`           varchar(255) DEFAULT NULL,
    `startdate`             timestamp  NOT NULL,
    `enddate`               timestamp  NOT NULL,
    `finalregistrationdate` timestamp  NOT NULL,
    `town`                  varchar(255) DEFAULT NULL,
    `country`               varchar(255) DEFAULT NULL,
    `price`                 varchar(255) DEFAULT NULL,
    `currency`              varchar(255) DEFAULT NULL,
    `OrgID`             bigint(20) NOT NULL,
    PRIMARY KEY (`courseId`),
    KEY `idx_name` (`name`),
    KEY `idx_orgid` (`OrgID`)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1;