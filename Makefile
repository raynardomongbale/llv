

generate_schema:
	@echo "generating schema"
	@cd backend && go generate ./schema/generate.sh

init_backend_dependencies:
	@echo "starting backend dependencies"
	@echo "starting db"
	@docker-compose -f backend/docker-compose.yml up -d
	@echo "db started"
	@cd backend && go mod download
	@echo "backend dependencies started"
	@echo "running migrations"
	@migrate -path /Users/raynard.omongbale/Clients/llv/client/backend/db/migrations/ -database "mysql://user:password@tcp(localhost:3307)/dev_apps" up 5
	@echo "migrations ran"


init_frontend_dependencies:
	@echo "starting frontend dependencies"
	@npm install
	@echo "frontend dependencies started"

start_backend:
	@echo "starting backend"
	@cd backend && go run main.go

start_frontend:
	@echo "starting frontend"
	@npm start
